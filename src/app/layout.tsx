import { CoreClientInitializer } from '#/components/core/core-client-initializer.component';

import { bodyFont, displayFont } from '#/assets/fonts/fonts';
import 'overlayscrollbars/overlayscrollbars.css';
import './globals.css';

import type { ReactNode } from 'react';

export default function RootLayout({ children }: { children: ReactNode }) {
  return (
    <html lang='en'>
      <body className={`${bodyFont.variable} ${displayFont.variable}`}>
        <CoreClientInitializer />
        {children}
      </body>
    </html>
  );
}
