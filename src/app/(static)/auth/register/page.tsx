'use client';

import { useCallback, useEffect, useMemo } from 'react';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';

import { UserRole } from '#/models/auth.model';
import { useBoundStore } from '#/hooks/use-store.hook';
import { BaseStaticScene } from '#/components/base/base-static-scene.component';
import { AuthRegisterForm } from '#/components/auth/auth-register-form.component';
import { AuthRegisterRoleTab } from '#/components/auth/auth-register-role-tab.component';

export default function AuthRegisterPage() {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const setOpenLogin = useBoundStore((state) => state.setOpenLogin);

  // If role query params is not user role then set it
  useEffect(() => {
    const role = searchParams.get('role');

    if (
      role === UserRole[1].toLowerCase() ||
      role === UserRole[2].toLowerCase()
    ) {
      return;
    }

    const href = `${pathname}?role=${UserRole[2].toLowerCase()}`;
    router.replace(href);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const selectedUserRole = useMemo(() => {
    const role = searchParams.get('role');
    const formattedRole: any =
      !!role && role.charAt(0).toUpperCase() + role.slice(1);

    return parseInt(UserRole[formattedRole]);
  }, [searchParams]);

  const handleChange = useCallback(
    (userRole: UserRole) => {
      const href = `${pathname}?role=${UserRole[userRole].toLowerCase()}`;
      router.push(href);
    },
    [router, pathname],
  );

  const handleLogin = useCallback(() => setOpenLogin(true), [setOpenLogin]);

  return (
    <BaseStaticScene id='auth-register'>
      <section className='mx-auto pt-4 max-w-[966px] w-full'>
        {!!selectedUserRole && (
          <div className='pb-12 flex flex-col justify-start items-start bg-backdrop/50 rounded-t-lg rounded-b-20px'>
            <AuthRegisterRoleTab
              className='mb-12 rounded-t-lg overflow-hidden'
              userRole={selectedUserRole}
              onChange={handleChange}
              onLogin={handleLogin}
            />
            <AuthRegisterForm
              className='px-4 lg:px-11'
              userRole={selectedUserRole}
            />
          </div>
        )}
      </section>
    </BaseStaticScene>
  );
}
