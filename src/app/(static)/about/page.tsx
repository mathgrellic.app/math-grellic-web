import Image from 'next/image';

import { BaseStaticScene } from '#/components/base/base-static-scene.component';

import aboutContent from '#/app/(static)/content/about-content.json';
import logoOnlyPng from '#/assets/images/logo-only.png';
import mgProfilePicPng from '#/assets/images/mg-profile-pic.png';
import mathSymbolsPng from '#/assets/images/math-symbols.png';

const headerStyle = { backgroundImage: `url(${mathSymbolsPng.src})` };

export default function AboutPage() {
  return (
    <BaseStaticScene title='About Us'>
      <section className='mb-56 mx-auto px-4 flex justify-between items-start max-w-static-full w-full'>
        <div className='flex justify-center max-w-[432px] w-full'>
          <Image
            src={logoOnlyPng}
            alt='logo only'
            width={logoOnlyPng.width}
            height={logoOnlyPng.height}
            quality={100}
          />
        </div>
        <div className='flex flex-col items-center max-w-[702px] w-full'>
          <h2 className='mb-6 leading-none'>{aboutContent.section1.title}</h2>
          <div
            className='text-lg text-justify'
            dangerouslySetInnerHTML={{ __html: aboutContent.section1.content }}
          ></div>
        </div>
      </section>
      <section className='flex flex-col items-center w-full'>
        <div
          style={headerStyle}
          className='relative flex flex-col items-center w-full bg-repeat-x bg-[center_16px] z-10'
        >
          <Image
            src={mgProfilePicPng}
            alt='myrhelle grecia profile'
            width={mgProfilePicPng.width}
            height={mgProfilePicPng.height}
            quality={100}
            className='mb-5'
          />
          <h2 className='leading-none'>{aboutContent.section2.header.name}</h2>
          <span className='text-primary text-21px font-display font-bold leading-tight'>
            {aboutContent.section2.header.title}
          </span>
        </div>
        <div className='flex flex-col items-center -mt-[170px] pt-48 pb-20 w-full bg-backdrop-light'>
          <div
            className='max-w-[800px] w-full text-lg text-justify'
            dangerouslySetInnerHTML={{ __html: aboutContent.section2.content }}
          />
        </div>
      </section>
    </BaseStaticScene>
  );
}
