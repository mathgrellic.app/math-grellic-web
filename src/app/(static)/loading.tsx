import { BasePageSpinner } from '#/components/base/base-spinner.component';

export default function Loading() {
  return <BasePageSpinner />;
}
