import { CorePageNotFound } from '#/components/core/core-page-not-found.component';

export default function NotFound() {
  return <CorePageNotFound href='/' linkLabel='Return home' />;
}
