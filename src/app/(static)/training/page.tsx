import { BaseStaticScene } from '#/components/base/base-static-scene.component';

export default function TrainingPage() {
  return <BaseStaticScene title='Training'></BaseStaticScene>;
}
