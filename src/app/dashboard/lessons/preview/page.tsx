'use client';

import { BaseScene } from '#/components/base/base-scene.component';
import { BasePageSpinner } from '#/components/base/base-spinner.component';
import { LessonSingle } from '#/components/lesson/lesson-single.component';

import { useLessonPreview } from '#/hooks/use-lesson-preview.hook';

export default function LessonPreviewPage() {
  const { lesson, titlePreview } = useLessonPreview();

  return lesson === undefined ? (
    <BasePageSpinner />
  ) : (
    <BaseScene title={titlePreview} breadcrumbsHidden isClose>
      {!lesson ? (
        <div className='pt-8 w-full text-center'>
          Lesson preview has expired.
        </div>
      ) : (
        <LessonSingle lesson={lesson} />
      )}
    </BaseScene>
  );
}
