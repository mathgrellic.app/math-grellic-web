'use client'

import { LESSONS_PATH } from '#/utils/paths.util';
import { BaseScene } from '#/components/base/base-scene.component';
import { BaseGroupLink } from '#/components/base/base-group-link.component';
import { LessonUpsertForm } from '#/components/lesson/lesson-upsert-form.component';

import type { GroupLink } from '#/models/base.model';

const links: GroupLink[] = [
  {
    href: LESSONS_PATH,
    label: 'Lesson List',
    icons: [{ name: 'plus', size: 16 }, { name: 'chalkboard-teacher' }],
  },
  {
    href: `${LESSONS_PATH}/schedule`,
    label: 'Schedule Lesson',
    icons: [{ name: 'plus', size: 16 }, { name: 'calendar' }],
  },
];

export default function LessonCreatePage() {
  return (
    <BaseScene
      title='Create a Lesson'
      headerRightContent={<BaseGroupLink links={links} />}
    >
      <LessonUpsertForm />
    </BaseScene>
  );
}
