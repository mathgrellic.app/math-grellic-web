import { LESSONS_PATH } from '#/utils/paths.util';
import { BaseScene } from '#/components/base/base-scene.component';
import { BaseGroupLink } from '#/components/base/base-group-link.component';

import type { GroupLink } from '#/models/base.model';

const links: GroupLink[] = [
  {
    href: LESSONS_PATH,
    label: 'Lesson List',
    icons: [{ name: 'plus', size: 16 }, { name: 'chalkboard-teacher' }],
  },
  {
    href: '/calendar',
    label: 'Calendar',
    icons: [{ name: 'calendar' }],
  },
];

export default function LessonSchedulePage() {
  return (
    <BaseScene
      title='Schedule a Lesson'
      headerRightContent={<BaseGroupLink links={links} />}
    ></BaseScene>
  );
}
