'use client';

import { CoreHeader } from '#/components/core/core-header.component';
import { CoreMain } from '#/components/core/core-main.component';
import { CoreSidebar } from '#/components/core/core-sidebar.component';

import type { ReactNode } from 'react';

export default function DashboardLayout({ children }: { children: ReactNode }) {
  return (
    <div className='flex justify-start items-start'>
      <CoreSidebar />
      <CoreMain id='main'>
        <CoreHeader />
        <div className='relative z-10'>{children}</div>
      </CoreMain>
    </div>
  );
}
