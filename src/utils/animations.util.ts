export const fadeInAnimation = {
  initial: { opacity: 0 },
  animate: { opacity: 1 },
  exit: { opacity: 0 },
  transition: { duration: 0.12, ease: 'linear' },
};

export const dropdownAnimation = {
  initial: { scale: 0.7 },
  animate: { scale: 1 },
  exit: { scale: 0.7 },
  transition: { duration: 0.16, ease: 'linear' },
};

export const modalAnimationBackdrop = {
  enter: 'ease-out duration-300',
  enterFrom: 'opacity-0',
  enterTo: 'opacity-100',
  leave: 'ease-in duration-200',
  leaveFrom: 'opacity-100',
  leaveTo: 'opacity-0',
};

export const modalAnimationPanel = {
  enterFrom: 'opacity-50 scale-50',
  enterTo: 'opacity-100 scale-100',
  leave: 'ease-in duration-200',
  leaveFrom: 'opacity-100 scale-100',
  leaveTo: 'opacity-50 scale-50',
};

export const menuAnimation = {
  enter: 'transition ease-out duration-100',
  enterFrom: 'transform opacity-0 scale-95',
  enterTo: 'transform opacity-100 scale-100',
  leave: 'transition ease-in duration-75',
  leaveFrom: 'transform opacity-100 scale-100',
  leaveTo: 'transform opacity-0 scale-95',
};

export const stepperAnimationVariants = {
  initial: (isForward: boolean) => ({
    x: isForward ? '100%' : '-100%',
    opacity: 0,
  }),
  animate: {
    x: '0%',
    opacity: 1,
  },
  exit: (isForward: boolean) => ({
    x: !isForward ? '100%' : '-100%',
    opacity: 0,
  }),
};

export const stepperAnimationTransition = {
  x: { type: 'spring', stiffness: 300, damping: 30 },
  opacity: { duration: 0.3 },
};
