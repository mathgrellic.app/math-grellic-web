export enum UserRole {
  Admin = 0,
  Teacher,
  Student,
}

export enum UserGender {
  Male = 0,
  Female,
}

export type AuthCredentials = {
  email: string;
  password: string;
};

export type AuthRegisterFormData = {
  email: string;
  password: string;
  confirmPassword: string;
  firstName: string;
  lastName: string;
  birthDate: Date;
  phoneNumber: string;
  gender: UserGender;
  middleName?: string;
};
