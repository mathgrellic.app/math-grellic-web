import { memo } from 'react';
import Image from 'next/image';
import cx from 'classix';

import homeContent from '#/app/(static)/content/home-content.json';
import iconBenefitFlexiblePng from '#/assets/images/icon-benefit-flexible.png';
import iconBenefitGamePng from '#/assets/images/icon-benefit-game.png';
import iconBenefitStarPng from '#/assets/images/icon-benefit-star.png';
import teacherWithSymbolsPng from '#/assets/images/illu-teacher-with-symbols.png';

import type { ComponentProps } from 'react';

const benefits = [
  {
    ...homeContent.section4.benefits[0],
    imgSrc: iconBenefitStarPng,
  },
  {
    ...homeContent.section4.benefits[1],
    imgSrc: iconBenefitGamePng,
  },
  {
    ...homeContent.section4.benefits[2],
    imgSrc: iconBenefitFlexiblePng,
  },
];

export const HomeSection4 = memo(function HomeSection4({
  className,
  ...moreProps
}: ComponentProps<'section'>) {
  return (
    <section
      className={cx(
        'mx-auto px-4 flex justify-between items-start max-w-static-full w-full',
        className,
      )}
      {...moreProps}
    >
      <div className='max-w-[706px] w-full'>
        <h2 className='mb-6'>{homeContent.section4.title}</h2>
        <div>
          {benefits.map(({ key, title, content, imgSrc }) => (
            <div key={key} className='flex items-center mb-11 last:mb-0'>
              <Image
                src={imgSrc}
                alt={title}
                width={imgSrc.width}
                height={imgSrc.height}
                quality={100}
                className='mr-4'
              />
              <div>
                <h3 className='mb-3.5 text-21px font-bold leading-none'>
                  {title}
                </h3>
                <p className='text-lg'>{content}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className='max-w-[446px] w-full'>
        <Image
          src={teacherWithSymbolsPng}
          alt='teacher with symbols'
          width={teacherWithSymbolsPng.width}
          height={teacherWithSymbolsPng.height}
          quality={100}
          className='translate-x-[18px]'
        />
      </div>
    </section>
  );
});
