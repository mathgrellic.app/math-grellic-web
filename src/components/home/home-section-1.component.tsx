'use client';

import { memo, useCallback } from 'react';
import Image from 'next/image';
import cx from 'classix';

import { useBoundStore } from '#/hooks/use-store.hook';
import { BaseButton } from '#/components/base/base-button.component';

import homeContent from '#/app/(static)/content/home-content.json';
import teacherWithLogoPng from '#/assets/images/illu-teacher-with-logo.png';

import type { ComponentProps } from 'react';

export const HomeSection1 = memo(function HomeSection1({
  className,
  ...moreProps
}: ComponentProps<'section'>) {
  const setOpenRegister = useBoundStore((state) => state.setOpenRegister);

  const handleGetStarted = useCallback(() => {
    setOpenRegister(true);
  }, [setOpenRegister]);

  return (
    <section
      className={cx(
        'mx-auto px-4 flex flex-col items-center max-w-static-full w-full',
        className,
      )}
      {...moreProps}
    >
      <div className='mb-6 text-center'>
        <h2 className='text-[40px]'>{homeContent.section1.title}</h2>
        <p className='text-lg'>{homeContent.section1.content}</p>
      </div>
      <BaseButton
        className='mb-16'
        variant='primary'
        rightIconName='rocket-launch'
        onClick={handleGetStarted}
      >
        Get Started
      </BaseButton>
      <Image
        src={teacherWithLogoPng}
        alt='teacher with logo'
        width={teacherWithLogoPng.width}
        height={teacherWithLogoPng.height}
        quality={100}
        priority
      />
    </section>
  );
});
