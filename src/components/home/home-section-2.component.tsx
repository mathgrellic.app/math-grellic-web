import { memo } from 'react';
import cx from 'classix';

import homeContent from '#/app/(static)/content/home-content.json';
import mathSymbolsPng from '#/assets/images/math-symbols.png';
import feat1Png from '#/assets/images/feat-1.png';
import feat2Png from '#/assets/images/feat-2.png';
import feat3Png from '#/assets/images/feat-3.png';

import type { ComponentProps } from 'react';

const headerStyle = { backgroundImage: `url(${mathSymbolsPng.src})` };

const features = [
  {
    ...homeContent.section2.features[0],
    style: { backgroundImage: `url(${feat1Png.src})` },
    contentStyle: { width: '282px' },
  },
  {
    ...homeContent.section2.features[1],
    style: { backgroundImage: `url(${feat2Png.src})` },
    contentStyle: { width: '314px' },
  },
  {
    ...homeContent.section2.features[2],
    style: { backgroundImage: `url(${feat3Png.src})` },
    contentStyle: { width: '330px' },
  },
];

export const HomeSection2 = memo(function HomeSection2({
  className,
  ...moreProps
}: ComponentProps<'section'>) {
  return (
    <section
      className={cx('flex flex-col items-center w-full', className)}
      {...moreProps}
    >
      <div
        style={headerStyle}
        className='flex justify-center items-end w-full h-44 bg-repeat-x bg-[-30px_top]'
      >
        <h2 className='text-lg font-bold'>{homeContent.section2.title}</h2>
      </div>
      <div className='px-4 flex justify-between items-end max-w-[calc(40px+theme(maxWidth.static-full))] w-full'>
        {features.map(({ key, title, content, contentStyle, style }) => (
          <div
            key={key}
            style={style}
            className='flex flex-col justify-end items-center pb-14 w-[410px] h-[500px] bg-no-repeat bg-bottom'
          >
            <h3 className='mb-3.5 text-21px font-bold leading-none'>{title}</h3>
            <p style={contentStyle} className='text-lg'>
              {content}
            </p>
          </div>
        ))}
      </div>
    </section>
  );
});
