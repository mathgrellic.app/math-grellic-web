'use client';

import { memo } from 'react';
import Image from 'next/image';
import cx from 'classix';

import { BaseLink } from '#/components/base/base-link.component';

import homeContent from '#/app/(static)/content/home-content.json';
import logoOnlyWithStudentPng from '#/assets/images/logo-only-with-student.png';

import type { ComponentProps } from 'react';

export const HomeSection3 = memo(function HomeSection3({
  className,
  ...moreProps
}: ComponentProps<'section'>) {
  return (
    <section
      className={cx(
        'mx-auto px-4 flex justify-between items-start max-w-static-full w-full',
        className,
      )}
      {...moreProps}
    >
      <div className='max-w-[435px] w-full'>
        <Image
          src={logoOnlyWithStudentPng}
          alt='logo with student'
          width={logoOnlyWithStudentPng.width}
          height={logoOnlyWithStudentPng.height}
          quality={100}
        />
      </div>
      <div className='flex flex-col items-center max-w-[702px] w-full'>
        <h2 className='mb-6'>{homeContent.section3.title}</h2>
        <div
          className='mb-5 text-lg text-justify'
          dangerouslySetInnerHTML={{ __html: homeContent.section3.content }}
        ></div>
        <BaseLink href='/about' rightIconName='arrow-circle-right'>
          Learn More
        </BaseLink>
      </div>
    </section>
  );
});
