import { memo } from 'react';
import Image from 'next/image';
import cx from 'classix';

import homeContent from '#/app/(static)/content/home-content.json';
import iconKeyExamPng from '#/assets/images/icon-key-exam.png';
import iconKeyExercisePng from '#/assets/images/icon-key-exercise.png';
import iconKeyFeedbackPng from '#/assets/images/icon-key-feedback.png';
import iconKeyLessonPng from '#/assets/images/icon-key-lesson.png';
import iconKeyRewardPng from '#/assets/images/icon-key-reward.png';
import iconKeyStatisticsPng from '#/assets/images/icon-key-statistics.png';

import type { ComponentProps } from 'react';

const keyFeatures = [
  {
    ...homeContent.section5.keyFeatures[0],
    imgSrc: iconKeyLessonPng,
  },
  {
    ...homeContent.section5.keyFeatures[1],
    imgSrc: iconKeyExercisePng,
  },
  {
    ...homeContent.section5.keyFeatures[2],
    imgSrc: iconKeyExamPng,
  },
  {
    ...homeContent.section5.keyFeatures[3],
    imgSrc: iconKeyFeedbackPng,
  },
  {
    ...homeContent.section5.keyFeatures[4],
    imgSrc: iconKeyStatisticsPng,
  },
  {
    ...homeContent.section5.keyFeatures[5],
    imgSrc: iconKeyRewardPng,
  },
];

export const HomeSection5 = memo(function HomeSection5({
  className,
  ...moreProps
}: ComponentProps<'section'>) {
  return (
    <section
      className={cx(
        'mx-auto px-4 flex flex-col items-center max-w-static-full w-full',
        className,
      )}
      {...moreProps}
    >
      <h2 className='mb-7'>{homeContent.section5.title}</h2>
      <div className='flex flex-col xl:flex-row justify-start xl:justify-between items-center xl:items-start flex-wrap'>
        {keyFeatures.map(({ key, title, content, imgSrc }) => (
          <div
            key={key}
            className='flex flex-col items-center mb-12 max-w-[577px]'
          >
            <Image
              src={imgSrc}
              alt={title}
              width={imgSrc.width}
              height={imgSrc.height}
              quality={100}
              className='mb-5'
            />
            <div className='flex flex-col items-center'>
              <h3 className='mb-5 text-center text-21px font-bold leading-none'>
                {title}
              </h3>
              <p className='text-lg text-justify'>{content}</p>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
});
