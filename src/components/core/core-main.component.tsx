'use client';

import cx from 'classix';

import { SidebarMode } from '#/models/base.model';
import { useBoundStore } from '#/hooks/use-store.hook';

import type { ComponentProps } from 'react';

export function CoreMain({
  className,
  children,
  ...moreProps
}: ComponentProps<'main'>) {
  const sidebarMode = useBoundStore((state) => state.sidebarMode);

  return (
    <main
      className={cx(
        'relative flex-1 w-full min-h-screen transition-[margin] duration-300',
        sidebarMode === SidebarMode.Collapsed && 'ml-[70px]',
        sidebarMode === SidebarMode.Expanded && 'ml-64',
        sidebarMode === SidebarMode.Hidden && 'ml-0',
        className,
      )}
      {...moreProps}
    >
      {children}
    </main>
  );
}
