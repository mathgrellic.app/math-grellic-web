'use client';

import { memo } from 'react';
import { usePathname } from 'next/navigation';
import cx from 'classix';

import { ABSOLUTE_REGISTER_PATH } from '#/utils/paths.util';
import { BaseButton } from '#/components/base/base-button.component';
import { CoreStaticNavItem } from './core-static-nav-item.component';

import type { ComponentProps } from 'react';
import type { NavLink } from '#/models/base.model';

type Props = ComponentProps<'nav'> & {
  links: NavLink[];
  onGetStarted?: () => void;
  onLogin?: () => void;
};

export const CoreStaticNav = memo(function CoreStaticNav({
  links,
  onGetStarted,
  onLogin,
  ...moreProps
}: Props) {
  const pathname = usePathname();

  return (
    <nav {...moreProps}>
      <ul className='flex items-center'>
        {links.map(({ name, label, href }) => (
          <li key={name}>
            <CoreStaticNavItem
              href={href}
              label={label}
              active={pathname === href}
            />
          </li>
        ))}
        <li>
          <BaseButton
            className={cx(
              '!px-3.5 !font-body !font-medium !tracking-normal',
              pathname === ABSOLUTE_REGISTER_PATH &&
                '!text-primary-focus-light',
            )}
            variant='link'
            onClick={onGetStarted}
          >
            Get Started
          </BaseButton>
        </li>
        <li className='pl-3.5'>
          <BaseButton rightIconName='door-open' size='sm' onClick={onLogin}>
            Sign In
          </BaseButton>
        </li>
      </ul>
    </nav>
  );
});
