import { memo } from 'react';
import { cx } from 'classix';

import { DASHBOARD_PATH } from '#/utils/paths.util';
import { BaseLink } from '#/components/base/base-link.component';

import type { ComponentProps } from 'react';

type Props = ComponentProps<'div'> & {
  href?: string;
  linkLabel?: string;
};

export const CorePageNotFound = memo(function CorePageNotFound({
  className,
  href = DASHBOARD_PATH,
  linkLabel = 'Return to dashboard',
  ...moreProps
}: Props) {
  return (
    <div
      className={cx(
        'flex flex-col justify-center items-center h-screen w-full pb-8 animate-fadeIn',
        className,
      )}
      {...moreProps}
    >
      <span className='mb-4 text-3xl text-primary font-bold font-display tracking-tighter'>
        Page Not Found
      </span>
      <span className='text-lg'>
        The page you are looking for does not exist.
      </span>
      <BaseLink href={href} className='!text-base'>
        {linkLabel}
      </BaseLink>
    </div>
  );
});
