import { memo } from 'react';
import Link from 'next/link';
import cx from 'classix';

import type { ComponentProps } from 'react';
import type { NavLink } from '#/models/base.model';

type Props = ComponentProps<typeof Link> &
  Omit<NavLink, 'href' | 'name'> & {
    active?: boolean;
  };

export const CoreStaticNavItem = memo(function CoreStaticNavItem({
  className,
  href,
  label,
  active,
  ...moreProps
}: Props) {
  return (
    <Link
      className={cx(
        'py-0.5 px-3.5 inline-block text-primary text-lg font-medium hover:text-primary-focus-light transition-colors',
        active && '!text-primary-focus-light',
        className,
      )}
      href={href}
      {...moreProps}
    >
      {label}
    </Link>
  );
});
