'use client';

import { memo, useEffect } from 'react';
import dayjs from 'dayjs';
import updateLocale from 'dayjs/plugin/updateLocale';
import localeData from 'dayjs/plugin/localeData';
import customParseFormat from 'dayjs/plugin/customParseFormat';

import { useBoundStore } from '#/hooks/use-store.hook';

// Initialize dayjs
// Use custom format for time
dayjs.extend(customParseFormat);
// Set global dayjs settings
dayjs.extend(localeData);
// Set dayjs start of week to monday
dayjs.extend(updateLocale);
dayjs.updateLocale('en', {
  weekStart: 1,
});

export const CoreClientInitializer = memo(function CoreClientInitializer() {
  // Rehydrate zustand store
  useEffect(() => {
    useBoundStore.persist.rehydrate();
  }, []);

  return null;
});
