'use client';

import { memo, useMemo } from 'react';
import { useSelectedLayoutSegment } from 'next/navigation';
import cx from 'classix';

import { DASHBOARD_PATH } from '#/utils/paths.util';
import { CoreNavItem } from './core-nav-item.component';

import type { ComponentProps } from 'react';
import type { NavLink } from '#/models/base.model';

type Props = ComponentProps<'nav'> & {
  links: NavLink[];
  isExpanded?: boolean;
};

export const CoreNav = memo(function CoreNav({
  className,
  links,
  isExpanded,
  ...moreProps
}: Props) {
  const segment = useSelectedLayoutSegment();
  const currentParentPath = useMemo(
    () => (!!segment ? `${DASHBOARD_PATH}/${segment}` : DASHBOARD_PATH),
    [segment],
  );

  return (
    <nav
      className={cx('w-full flex items-center overflow-hidden', className)}
      {...moreProps}
    >
      <ul className='flex flex-col w-full'>
        {links.map(({ name, label, href, iconName, size }) => (
          <li key={name}>
            <CoreNavItem
              href={href}
              label={label}
              iconName={iconName}
              size={size}
              active={currentParentPath === href}
              isExpanded={isExpanded}
            />
          </li>
        ))}
      </ul>
    </nav>
  );
});
