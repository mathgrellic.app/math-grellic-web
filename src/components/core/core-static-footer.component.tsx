'use client';

import { memo, useCallback } from 'react';
import { usePathname } from 'next/navigation';
import Link from 'next/link';
import Image from 'next/image';

import { ABSOLUTE_REGISTER_PATH } from '#/utils/paths.util';
import { useBoundStore } from '#/hooks/use-store.hook';
import { BaseButton } from '#/components/base/base-button.component';

import studentWithNumbersPng from '#/assets/images/illu-student-with-numbers.png';
import logoColPng from '#/assets/images/logo-col.png';
import gridPng from '#/assets/images/grid.png';

import type { ComponentProps } from 'react';

const bgStyle = { backgroundImage: `url(${gridPng.src})` };
const currentYear = new Date().getFullYear();

export const CoreStaticFooter = memo(function CoreStaticFooter({
  className,
  ...moreProps
}: ComponentProps<'footer'>) {
  const pathname = usePathname();
  const setOpenRegister = useBoundStore((state) => state.setOpenRegister);

  const handleGetStarted = useCallback(() => {
    // If already on register page, just scroll to top
    if (pathname === ABSOLUTE_REGISTER_PATH) {
      const element = document.getElementById('main');
      !!element && element.scrollIntoView({ behavior: 'smooth' });
      return;
    }

    setOpenRegister(true);
  }, [pathname, setOpenRegister]);

  return (
    <footer className='relative pb-5 w-full' {...moreProps}>
      <div
        style={bgStyle}
        className='absolute w-full h-full bottom-0 bg-backdrop-focus'
      >
        <div className='w-full h-full bg-gradient-to-t from-transparent to-backdrop' />
      </div>
      <div className='relative mx-auto flex flex-col items-center max-w-static-full z-10'>
        <div className='flex justify-between items-center mb-40'>
          <Image
            src={studentWithNumbersPng}
            alt='student with numbers'
            width={studentWithNumbersPng.width}
            height={studentWithNumbersPng.height}
            quality={100}
            className='-translate-x-4'
          />
          <div className='pt-6 flex flex-col items-center'>
            <h2 className='mb-7 text-[44px] font-bold'>
              Begin your journey with{' '}
              <i className='text-secondary'>
                {process.env.NEXT_PUBLIC_META_TITLE}
              </i>{' '}
              today.
            </h2>
            <BaseButton
              variant='primary'
              rightIconName='rocket-launch'
              onClick={handleGetStarted}
            >
              Get Started
            </BaseButton>
          </div>
        </div>
        <Link
          href='/'
          className='inline-block mb-10 transition-all hover:brightness-110'
        >
          <Image
            src={logoColPng}
            alt='footer logo'
            width={logoColPng.width}
            height={logoColPng.height}
            quality={100}
          />
        </Link>
        <span className='text-lg font-medium leading-none'>
          © {currentYear} Math Grellic. All rights reserved
        </span>
      </div>
    </footer>
  );
});
