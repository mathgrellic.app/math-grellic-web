import { memo, useMemo } from 'react';
import dayjs from 'dayjs';
import cx from 'classix';

import { BaseDivider } from '#/components/base/base-divider.component';

import type { ComponentProps } from 'react';

type Props = ComponentProps<'div'> & { dateTime: Date; isCompact?: boolean };

export const CoreClock = memo(function CoreClock({
  className,
  dateTime,
  isCompact,
  ...moreProps
}: Props) {
  const time = useMemo(() => dayjs(dateTime).format('hh:mm A'), [dateTime]);
  const date = useMemo(
    () => dayjs(dateTime).format('MMM DD, YYYY'),
    [dateTime],
  );
  const dayName = useMemo(() => dayjs(dateTime).format('dddd'), [dateTime]);

  return (
    <div
      className={cx(
        'flex items-center gap-2.5 transition-[gap]',
        isCompact && '!gap-0',
        className,
      )}
      {...moreProps}
    >
      {/* TEMP MAKE CLOCK REALTIME */}
      <div className='uppercase leading-none font-medium text-primary'>
        {time}
      </div>
      <div
        className={cx(
          'h-full opacity-100 transition-opacity duration-300',
          isCompact && '!opacity-0',
        )}
      >
        <BaseDivider vertical />
      </div>
      <div
        className={cx(
          'w-[104px] opacity-100 flex flex-col items-start uppercase leading-none font-medium text-primary whitespace-nowrap overflow-hidden transition-[width,opacity] duration-300',
          isCompact && '!w-0 !opacity-0',
        )}
      >
        <span>{dayName}</span>
        <span>{date}</span>
      </div>
    </div>
  );
});
