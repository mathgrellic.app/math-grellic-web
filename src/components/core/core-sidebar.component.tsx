'use client';

import { memo, useCallback, useMemo } from 'react';
import cx from 'classix';

import { NavLink, SidebarMode } from '#/models/base.model';
import { DASHBOARD_PATH } from '#/utils/paths.util';
import { useBoundStore } from '#/hooks/use-store.hook';
import { BaseIconButton } from '#/components/base/base-icon-button.component';
import { CoreLogo } from './core-logo.component';
import { CoreNav } from './core-nav.component';

import gridSmPng from '#/assets/images/grid-sm.png';
import navLinksDashboardJson from '#/utils/nav-links-dashboard.json';

import type { ComponentProps } from 'react';

const bgStyle = { backgroundImage: `url(${gridSmPng.src})` };

const BASE_PATH = '';

export const CoreSidebar = memo(function CoreSidebar({
  className,
  ...moreProps
}: ComponentProps<'aside'>) {
  const sidebarMode = useBoundStore((state) => state.sidebarMode);
  const setSidebarMode = useBoundStore((state) => state.setSidebarMode);

  const modeIconName = useMemo(() => {
    switch (sidebarMode) {
      case SidebarMode.Collapsed:
        return 'arrows-out-line-horizontal';
      case SidebarMode.Expanded:
        return 'arrows-in-line-horizontal';
      default:
        return null;
    }
  }, [sidebarMode]);

  const navLinks = useMemo(() => {
    return navLinksDashboardJson.teacher.map(({ href, ...t }) => ({
      ...t,
      href: href === '/' ? DASHBOARD_PATH : `${DASHBOARD_PATH}${href}`,
    })) as NavLink[];
  }, []);

  const handleSwitchMode = useCallback(() => {
    setSidebarMode(sidebarMode === 0 ? 1 : 0);
  }, [sidebarMode, setSidebarMode]);

  return (
    <aside
      style={bgStyle}
      className={cx(
        'fixed top-0 left-0 shrink-0 h-screen bg-backdrop-light z-50 transition-[width] duration-300',
        sidebarMode === SidebarMode.Collapsed && 'w-[70px]',
        sidebarMode === SidebarMode.Expanded && 'w-64',
        sidebarMode === SidebarMode.Hidden && 'w-0',
        className,
      )}
      {...moreProps}
    >
      <div className='absolute top-0 right-0 w-3/4 h-full bg-gradient-to-r from-transparent to-white border-r border-primary' />
      <div className='py-3 relative flex flex-col justify-between w-full h-screen z-10'>
        <div className='relative pt-2.5 grow-0 shrink-0 z-10'>
          <CoreLogo
            href={DASHBOARD_PATH}
            isExpanded={sidebarMode === SidebarMode.Expanded}
          />
        </div>
        <CoreNav
          links={navLinks}
          className='absolute left-0 top-1/2 -translate-y-1/2 z-0'
          isExpanded={sidebarMode === SidebarMode.Expanded}
        />
        <div className='relative flex justify-center items-center grow-0 shrink-0 z-10'>
          {!!modeIconName && (
            <BaseIconButton
              name={modeIconName}
              className='!w-full'
              variant='link'
              onClick={handleSwitchMode}
            />
          )}
        </div>
      </div>
    </aside>
  );
});
