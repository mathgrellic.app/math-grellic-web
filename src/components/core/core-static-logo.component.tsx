import { memo } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import cx from 'classix';

import logoPng from '#/assets/images/logo.png';

import type { ComponentProps } from 'react';

type Props = ComponentProps<typeof Link> & {
  isHome?: boolean;
  isCompact?: boolean;
};

export const CoreStaticLogo = memo(function CoreStaticLogo({
  className,
  isHome,
  isCompact,
  ...moreProps
}: Props) {
  return (
    <Link
      className={cx(
        'py-2 inline-block origin-left transition-all hover:brightness-110',
        isCompact && 'scale-90',
        className,
      )}
      {...moreProps}
    >
      <Image
        src={logoPng}
        alt='logo'
        width={logoPng.width}
        height={logoPng.height}
        quality={100}
        priority
      />
      {isHome && (
        <h1 className='absolute invisible'>
          {process.env.NEXT_PUBLIC_META_TITLE}
        </h1>
      )}
    </Link>
  );
});
