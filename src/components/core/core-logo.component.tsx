'use client';

import { memo } from 'react';
import Link from 'next/link';
import cx from 'classix';

import logoPng from '#/assets/images/logo.png';

import type { ComponentProps } from 'react';

type Props = ComponentProps<typeof Link> & {
  isExpanded?: boolean;
};

const logoStyle = {
  backgroundImage: `url(${logoPng.src})`,
  backgroundSize: '214px 37px',
  backgroundPositionX: 'left',
};

export const CoreLogo = memo(function CoreLogo({
  className,
  isExpanded,
  ...moreProps
}: Props) {
  return (
    <Link
      className={cx(
        'inline-flex items-center py-1 pl-4 w-full transition-all hover:brightness-110',
        className,
      )}
      {...moreProps}
    >
      <div
        style={logoStyle}
        className={cx(
          'w-12 h-[37px] transition-[width] duration-200',
          isExpanded && '!w-[214px]',
        )}
      />
    </Link>
  );
});
