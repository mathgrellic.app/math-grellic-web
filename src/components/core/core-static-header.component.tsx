'use client';

import { memo, useCallback, useState } from 'react';
import { usePathname } from 'next/navigation';
import { useMotionValueEvent, useScroll } from 'framer-motion';
import cx from 'classix';

import { ABSOLUTE_REGISTER_PATH } from '#/utils/paths.util';
import { useBoundStore } from '#/hooks/use-store.hook';
import { CoreStaticLogo } from './core-static-logo.component';
import { CoreStaticNav } from './core-static-nav.component';

import navLinksStaticJson from '#/utils/nav-links-static.json';

import type { ComponentProps } from 'react';

const SCROLL_Y_THRESHOLD = 40;

export const CoreStaticHeader = memo(function CoreStaticHeader({
  className,
  ...moreProps
}: ComponentProps<'header'>) {
  const pathname = usePathname();
  const { scrollY } = useScroll();
  const setOpenRegister = useBoundStore((state) => state.setOpenRegister);
  const setOpenLogin = useBoundStore((state) => state.setOpenLogin);
  const [isScrollTop, setIsScrollTop] = useState(true);
  const isHome = pathname === '/';

  useMotionValueEvent(scrollY, 'change', (latest) => {
    setIsScrollTop(latest <= SCROLL_Y_THRESHOLD);
  });

  const handleGetStarted = useCallback(() => {
    // If already on register page, just scroll to top
    if (pathname === ABSOLUTE_REGISTER_PATH) {
      const element = document.getElementById('main');
      !!element && element.scrollIntoView({ behavior: 'smooth' });
      return;
    }

    setOpenRegister(true);
  }, [pathname, setOpenRegister]);

  const handleLogin = useCallback(() => setOpenLogin(true), [setOpenLogin]);

  return (
    <header
      className={cx('fixed left-0 top-0 w-full z-40', className)}
      {...moreProps}
    >
      <div
        className={cx(
          'absolute top-0 left-0 w-full h-full border-b border-primary-border/40 bg-backdrop/60 opacity-0 transition-opacity backdrop-blur',
          !isScrollTop && 'opacity-100',
        )}
      />
      <div
        className={cx(
          'relative mx-auto px-4 flex justify-between items-center max-w-static-full w-full h-20 transition-[height] z-10',
          !isScrollTop && '!h-14',
        )}
      >
        <CoreStaticLogo href='/' isHome={isHome} isCompact={!isScrollTop} />
        <CoreStaticNav
          links={navLinksStaticJson}
          onGetStarted={handleGetStarted}
          onLogin={handleLogin}
        />
      </div>
    </header>
  );
});
