'use client';

import { memo } from 'react';
import Link from 'next/link';
import cx from 'classix';

import { BaseIcon } from '#/components/base/base-icon.component';

import type { ComponentProps } from 'react';
import type { NavLink } from '#/models/base.model';

type Props = ComponentProps<typeof Link> &
  Omit<NavLink, 'href' | 'name'> & {
    active?: boolean;
    isExpanded?: boolean;
  };

export const CoreNavItem = memo(function CoreNavItem({
  className,
  href,
  label,
  iconName,
  size,
  active,
  isExpanded,
  ...moreProps
}: Props) {
  return (
    <Link
      className={cx(
        'relative flex items-center gap-5 px-4 h-12 text-primary hover:text-primary-focus-light font-body font-medium text-lg transition-colors',
        active && '!text-primary-focus-light',
        className,
      )}
      href={href}
      {...moreProps}
    >
      <div className='shrink-0 flex justify-center items-center w-9'>
        {!!iconName && <BaseIcon name={iconName} size={size || 32} />}
      </div>
      <span
        className={cx(
          'opacity-0 transition-opacity duration-300',
          isExpanded && '!opacity-100',
        )}
      >
        {label}
      </span>
      <div
        className={cx(
          'absolute -right-[1px] top-1/2 -translate-y-1/2 translate-x-4 w-0 h-0 border-t-[11px] border-r-[15px] border-b-[11px] border-solid border-transparent border-r-primary transition-transform',
          active && '!translate-x-0',
        )}
      >
        <div className='absolute left-0.5 -top-2.5 w-0 h-0 border-t-[10px] border-r-[14px] border-b-[10px] border-solid border-transparent border-r-backdrop' />
      </div>
    </Link>
  );
});
