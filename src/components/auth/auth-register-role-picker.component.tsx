import { memo, useCallback } from 'react';
import Image from 'next/image';
import cx from 'classix';

import { UserRole } from '#/models/auth.model';
import { BaseSurface } from '#/components/base/base-surface.component';
import { BaseSpinner } from '#/components/base/base-spinner.component';

import selectStudentPng from '#/assets/images/select-student.png';
import selectTeacherPng from '#/assets/images/select-teacher.png';

import type { ComponentProps } from 'react';

type Props = ComponentProps<'div'> & {
  loading?: boolean;
  onRoleChange: (role: UserRole) => void;
};

type ButtonProps = ComponentProps<'button'> & {
  label: string;
};

const Button = memo(function Button({
  className,
  label,
  children,
  ...moreProps
}: ButtonProps) {
  return (
    <button
      className={cx(
        'group w-[240px] active:scale-95 transition-transform',
        className,
      )}
      {...moreProps}
    >
      <div className='relative pt-5 w-full group-hover:-translate-y-5 transition-transform'>
        {children}
        <BaseSurface
          className={`!pt-[170px] !pb-3 flex justify-center w-full border border-primary-focus/50 font-display font-bold text-primary text-21px
            tracking-tighter leading-none group-hover:text-primary-focus drop-shadow-primary group-hover:drop-shadow-primary-focus transition-all`}
          rounded='base'
        >
          {label}
        </BaseSurface>
      </div>
    </button>
  );
});

export const AuthRegisterRolePicker = memo(function AuthRegisterRolePicker({
  className,
  loading,
  onRoleChange,
  ...moreProps
}: Props) {
  const handleRoleChange = useCallback(
    (role: UserRole) => () => onRoleChange(role),
    [onRoleChange],
  );

  return (
    <div className={cx('relative pb-16', className)} {...moreProps}>
      {loading && (
        <div className='absolute top-0 left-0 w-full h-full flex justify-center items-center z-20'>
          <BaseSpinner />
        </div>
      )}
      <div
        className={cx(
          'flex flex-col items-center transition-opacity',
          loading && 'opacity-50',
        )}
      >
        <div className='mb-12 flex flex-col items-center'>
          <h2 className='mb-2 text-28px'>Which one are you?</h2>
          <span className='text-lg'>
            Select your classroom role to get started
          </span>
        </div>
        <div className='flex justify-center items-start gap-10'>
          <Button
            label='Student'
            onClick={handleRoleChange(UserRole.Student)}
            disabled={loading}
          >
            <Image
              src={selectStudentPng}
              alt='select student'
              width={selectStudentPng.width}
              height={selectStudentPng.height}
              quality={100}
              className='absolute top-0 left-1/2 -translate-x-1/2 z-10'
            />
          </Button>
          <Button
            label='Teacher'
            onClick={handleRoleChange(UserRole.Teacher)}
            disabled={loading}
          >
            <Image
              src={selectTeacherPng}
              alt='select teacher'
              width={selectTeacherPng.width}
              height={selectTeacherPng.height}
              quality={100}
              className='absolute top-0 left-1/2 -translate-x-1/2 z-10'
            />
          </Button>
        </div>
      </div>
    </div>
  );
});
