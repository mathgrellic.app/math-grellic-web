import { memo, useCallback, useMemo } from 'react';
import { Tab } from '@headlessui/react';
import cx from 'classix';

import { UserRole } from '#/models/auth.model';

import type { ComponentProps } from 'react';

type Props = Omit<ComponentProps<'div'>, 'onChange'> & {
  userRole: UserRole;
  onChange?: (role: UserRole) => void;
  onLogin?: () => void;
};

const tabs = [
  { key: `tab-${UserRole[2].toLowerCase()}`, label: UserRole[2] },
  { key: `tab-${UserRole[1].toLowerCase()}`, label: UserRole[1] },
];

export const AuthRegisterRoleTab = memo(function AuthRegisterRoleTab({
  className,
  userRole,
  onChange,
  onLogin,
  ...moreProps
}: Props) {
  const selectedIndex = useMemo(() => (userRole === 2 ? 0 : 1), [userRole]);

  const handleChange = useCallback(
    (index: number) => {
      !!onChange && onChange(index === 0 ? 2 : 1);
    },
    [onChange],
  );

  return (
    <div className={cx('w-full', className)} {...moreProps}>
      <Tab.Group selectedIndex={selectedIndex} onChange={handleChange}>
        <Tab.List className='flex items-center p-1.5 w-full bg-backdrop-gray/60 border-b-2 border-accent/10'>
          {tabs.map(({ key, label }) => (
            <Tab
              key={key}
              className={({ selected }) =>
                cx(
                  `flex-1 w-full py-3 font-display text-lg leading-none tracking-tighter rounded bg-transparent text-accent/70
                    hover:text-primary-focus ring-1 ring-transparent focus:ring-primary focus:outline-0 transition-all`,
                  selected && '!bg-backdrop !text-primary',
                )
              }
            >
              {label}
            </Tab>
          ))}
        </Tab.List>
        <Tab.Panels className='pt-8 px-4 lg:px-11 w-full'>
          <h1 className='mb-2'>
            Sign up as a {UserRole[userRole].toLowerCase()}
          </h1>
          <p className='max-w-[600px] text-lg'>
            Complete the form and unlock the vast features of Math Grellic. If
            you&apos;re already a member, simply{' '}
            <button
              className='text-primary hover:text-primary-focus-light'
              onClick={onLogin}
            >
              sign in using your existing credentials
            </button>
            .
          </p>
        </Tab.Panels>
      </Tab.Group>
    </div>
  );
});
