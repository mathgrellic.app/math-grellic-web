'use client';

import { memo, useCallback } from 'react';
import { usePathname } from 'next/navigation';

import { ABSOLUTE_REGISTER_PATH } from '#/utils/paths.util';
import { useBoundStore } from '#/hooks/use-store.hook';
import { BaseModal } from '#/components/base/base-modal.component';
import { AuthLoginForm } from './auth-login-form.component';

import type { ComponentProps } from 'react';

type Props = Omit<ComponentProps<typeof BaseModal>, 'open' | 'onClose'> & {};

export const AuthLoginModal = memo(function AuthLoginModal(props: Props) {
  const pathname = usePathname();
  const openLogin = useBoundStore((state) => state.openLogin);
  const setOpenLogin = useBoundStore((state) => state.setOpenLogin);
  const setOpenRegister = useBoundStore((state) => state.setOpenRegister);

  const closeModal = useCallback(() => {
    setOpenLogin(false);
  }, [setOpenLogin]);

  const handleRegister = useCallback(() => {
    // If already on register page, just scroll to top
    if (pathname === ABSOLUTE_REGISTER_PATH) {
      const element = document.getElementById('main');
      !!element && element.scrollIntoView({ behavior: 'smooth' });
      closeModal();
      return;
    }

    setOpenRegister(true);
  }, [pathname, closeModal, setOpenRegister]);

  return (
    <BaseModal
      className='!px-0 !pb-0 !border-0'
      size='sm'
      {...props}
      open={!!openLogin}
      onClose={closeModal}
    >
      <AuthLoginForm onRegister={handleRegister} />
    </BaseModal>
  );
});
