'use client';

import { memo, useCallback, useEffect, useState } from 'react';
import { usePathname, useRouter } from 'next/navigation';

import { ABSOLUTE_REGISTER_PATH } from '#/utils/paths.util';
import { UserRole } from '#/models/auth.model';
import { useBoundStore } from '#/hooks/use-store.hook';
import { BaseModal } from '#/components/base/base-modal.component';
import { AuthRegisterRolePicker } from './auth-register-role-picker.component';

import type { ComponentProps } from 'react';

type Props = Omit<ComponentProps<typeof BaseModal>, 'open' | 'onClose'> & {};

export const AuthRegisterModal = memo(function AuthRegisterModal(props: Props) {
  const router = useRouter();
  const pathname = usePathname();
  const openRegister = useBoundStore((state) => state.openRegister);
  const setOpenRegister = useBoundStore((state) => state.setOpenRegister);
  const [loading, setLoading] = useState(false);

  const closeModal = useCallback(
    (forced?: boolean) => {
      if (!forced && loading) {
        return;
      }
      setLoading(false);
      setOpenRegister(false);
    },
    [loading, setOpenRegister],
  );

  const handleRoleChange = useCallback(
    (role: UserRole) => {
      const keyRole = UserRole[role].toLowerCase();
      router.push(`${ABSOLUTE_REGISTER_PATH}?role=${keyRole}`);
      setLoading(true);
    },
    [router],
  );

  useEffect(() => {
    if (pathname !== ABSOLUTE_REGISTER_PATH) {
      return;
    }

    closeModal(true);
  }, [pathname, closeModal]);

  return (
    <BaseModal {...props} open={!!openRegister} onClose={closeModal}>
      <AuthRegisterRolePicker
        onRoleChange={handleRoleChange}
        loading={loading}
      />
    </BaseModal>
  );
});
