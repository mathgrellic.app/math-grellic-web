'use client';

import { ComponentProps, memo, useCallback, useState } from 'react';
import { useRouter } from 'next/navigation';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import z from 'zod';
import cx from 'classix';

import { DASHBOARD_PATH } from '#/utils/paths.util';
import { BaseButton } from '#/components/base/base-button.component';
import { BaseControlledInput } from '#/components/base/base-input.component';
import { BaseControlledPasswordInput } from '#/components/base/base-password-input.component';

import gridWhiteSmPng from '#/assets/images/grid-white-sm.png';
import logoOnlySmPng from '#/assets/images/logo-only-sm.png';

import type { AuthCredentials } from '#/models/auth.model';

type Props = ComponentProps<'div'> & {
  onRegister?: () => void;
};

const bgStyle = { backgroundImage: `url(${gridWhiteSmPng.src})` };

const schema = z.object({
  email: z.string().email('Provide your email address'),
  password: z.string().min(1, 'Provide your password'),
});

const defaultValues = {
  email: '',
  password: '',
};

export const AuthLoginForm = memo(function AuthLoginForm({
  className,
  onRegister,
  ...moreProps
}: Props) {
  const router = useRouter();
  const {
    control,
    formState: { errors, isSubmitting },
    handleSubmit,
    getValues,
  } = useForm<AuthCredentials>({
    shouldFocusError: false,
    defaultValues,
    resolver: zodResolver(schema),
  });

  const [showPassword, setShowPassword] = useState(false);

  const submitForm = useCallback(
    async (data: AuthCredentials) => {
      try {
        // TEMP
        console.log('success', data);
        router.push(DASHBOARD_PATH);
      } catch (error) {
        console.log('error', error);
      }
    },
    [router],
  );

  return (
    <div className={cx('w-full', className)} {...moreProps}>
      <div className='px-14 pb-14'>
        <div className='mb-9'>
          <h2 className='mb-2'>Welcome Back!</h2>
          <p className='text-lg'>
            Sign in to embark on an exciting adventure. If you are new,
            don&apos;t worry,{' '}
            <button
              className='text-primary hover:text-primary-focus-light'
              onClick={onRegister}
            >
              signing up is quick and easy
            </button>
            .
          </p>
        </div>
        <form onSubmit={handleSubmit(submitForm)}>
          <fieldset className='mb-6 flex flex-col items-center gap-6'>
            <BaseControlledInput
              type='email'
              name='email'
              label='Email'
              control={control}
              fullWidth
            />
            <BaseControlledPasswordInput
              name='password'
              label='Password'
              control={control}
              showPassword={showPassword}
              onShowPassword={setShowPassword}
              fullWidth
            />
          </fieldset>
          <BaseButton
            type='submit'
            className='px-8 !h-16 w-full !text-xl'
            size='base'
          >
            Sign In
          </BaseButton>
        </form>
      </div>
      <div className='w-full bg-gradient-to-b from-primary to-primary-focus-light'>
        <div
          style={bgStyle}
          className='py-6 flex justify-center items-center w-full'
        >
          <Image
            src={logoOnlySmPng}
            alt='logo only sm'
            className='drop-shadow-sm'
            width={logoOnlySmPng.width}
            height={logoOnlySmPng.height}
            quality={100}
            priority
          />
        </div>
      </div>
    </div>
  );
});
