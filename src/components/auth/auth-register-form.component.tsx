'use client';

import { memo, useCallback, useState } from 'react';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import z from 'zod';
import isMobilePhone from 'validator/lib/isMobilePhone';
import cx from 'classix';

import { UserGender, UserRole } from '#/models/auth.model';
import { BaseButton } from '#/components/base/base-button.component';
import { BaseControlledCheckbox } from '#/components/base/base-checkbox.component';
import { BaseControlledDatePicker } from '#/components/base/base-date-picker.component';
import {
  BaseControlledInput,
  BaseControlledPhoneInput,
} from '#/components/base/base-input.component';
import { BaseControlledPasswordInput } from '#/components/base/base-password-input.component';
import { BaseControlledSelect } from '#/components/base/base-select.component';
import { BaseDivider } from '#/components/base/base-divider.component';

import type { ComponentProps } from 'react';
import type { AuthRegisterFormData } from '#/models/auth.model';
import type { SelectOption } from '#/models/base.model';

type Props = ComponentProps<'div'> & {
  userRole: UserRole;
};

const genders: SelectOption[] = [
  {
    label: UserGender[UserGender.Male],
    value: UserGender.Male,
  },
  {
    label: UserGender[UserGender.Female],
    value: UserGender.Female,
  },
];

const schema = z
  .object({
    email: z.string().email('Provide your email address'),
    password: z
      .string()
      .min(8, 'Password should be minimum of 8 characters')
      .max(100, 'Password is too long'),
    confirmPassword: z.string(),
    firstName: z
      .string()
      .min(2, 'Name is too short')
      .max(50, 'Name is too long'),
    lastName: z
      .string()
      .min(2, 'Name is too short')
      .max(50, 'Name is too long'),
    middleName: z
      .string()
      .min(1, 'Name is too short')
      .max(50, 'Name is too long')
      .optional(),
    birthDate: z
      .date({ required_error: 'Provide your date of birth' })
      .min(new Date('1900-01-01'), 'Date of birth is too old')
      .max(new Date(), 'Date of birth is too young'),
    phoneNumber: z
      .string()
      .refine((value) => isMobilePhone(value.replace(/[^0-9]/g, ''), 'en-PH'), {
        message: 'Phone number is invalid',
      }),
    gender: z.nativeEnum(UserGender, {
      required_error: 'Provide your gender',
    }),
    agreeTerms: z.boolean(),
  })
  .refine((data) => data.password === data.confirmPassword, {
    message: 'Password does not match',
    path: ['confirmPassword'],
  });

const defaultValues: any = {
  email: '',
  password: '',
  confirmPassword: '',
  firstName: '',
  lastName: '',
  middleName: '',
  birthDate: undefined,
  phoneNumber: '',
  gender: undefined,
};

export const AuthRegisterForm = memo(function AuthRegisterForm({
  className,
  userRole,
  ...moreProps
}: Props) {
  const {
    control,
    formState: { errors, isSubmitting },
    handleSubmit,
    getValues,
    reset,
  } = useForm<AuthRegisterFormData>({
    shouldFocusError: false,
    defaultValues,
    resolver: zodResolver(schema),
  });

  const [showPassword, setShowPassword] = useState(false);

  const handleReset = useCallback(() => reset(), [reset]);

  const submitForm = useCallback(async (data: AuthRegisterFormData) => {
    try {
      console.log('success');
    } catch (error) {
      console.log('error', error);
    }
  }, []);

  return (
    <div className={cx('w-full', className)} {...moreProps}>
      <form
        onSubmit={handleSubmit(submitForm, (errors) =>
          console.log(getValues()),
        )}
      >
        <fieldset className='flex flex-wrap justify-center lg:grid lg:grid-cols-3 gap-5'>
          <BaseControlledInput
            label='First Name'
            name='firstName'
            control={control}
            asterisk
          />
          <BaseControlledInput
            label='Last Name'
            name='lastName'
            control={control}
            asterisk
          />
          <BaseControlledInput
            label='Middle Name'
            name='middleName'
            control={control}
            asterisk
          />
          <BaseControlledSelect
            name='gender'
            label='Gender'
            options={genders}
            control={control}
            asterisk
          />
          <BaseControlledDatePicker
            name='birthDate'
            label='Date of Birth'
            control={control}
            iconName='calendar'
            asterisk
          />
          <BaseControlledPhoneInput
            label='Phone Number'
            name='phoneNumber'
            control={control}
            asterisk
          />
        </fieldset>
        <BaseDivider className='my-4' />
        <fieldset className='grid grid-cols-1 md:flex justify-center lg:grid lg:grid-cols-3 gap-5 place-items-center lg:place-items-start'>
          <BaseControlledInput
            type='email'
            name='email'
            label='Email'
            control={control}
            asterisk
          />
          <BaseControlledPasswordInput
            name='password'
            label='Password'
            control={control}
            showPassword={showPassword}
            onShowPassword={setShowPassword}
            asterisk
          />
          <BaseControlledInput
            type={showPassword ? 'text' : 'password'}
            name='confirmPassword'
            label='Confirm Password'
            control={control}
          />
        </fieldset>
        <div className='mt-8 px-4 pt-2 pb-2.5 w-full border border-accent/40 rounded-md'>
          <BaseControlledCheckbox
            name='agreeTerms'
            label='By checking this box, I agree to the terms and conditions.
              These guidelines cover app usage, content, and privacy. Please review
              them before enjoying the edutainment experience.'
            control={control}
          />
        </div>
        <div className='mt-8 flex justify-between items-center'>
          <BaseButton
            variant='link'
            rightIconName='arrow-counter-clockwise'
            onClick={handleReset}
          >
            Reset Fields
          </BaseButton>
          <BaseButton type='submit' className='px-8 !h-16 !text-xl' size='base'>
            Complete Sign Up
          </BaseButton>
        </div>
      </form>
    </div>
  );
});
