import { memo } from 'react';
import { useFormContext } from 'react-hook-form';

import { BaseControlledDatePicker } from '#/components/base/base-date-picker.component';
import { BaseControlledTimePicker } from '#/components/base/base-time-picker.component';

import type { LessonUpsertFormData } from '#/models/lesson.model';

const calendarSelectorProps = {
  minDate: new Date(`${new Date().getFullYear() - 5}-01-01`),
  maxDate: new Date(`${new Date().getFullYear() + 5}-12-31`),
};

export const LessonUpsertFormStep2 = memo(function LessonUpsertFormStep2() {
  const { control } = useFormContext<LessonUpsertFormData>();

  return (
    <div>
      <fieldset className='flex gap-5 flex-wrap'>
        <div className='flex justify-between items-start gap-5 w-full'>
          <BaseControlledDatePicker
            name='startDate'
            label='Start Date'
            control={control}
            iconName='calendar'
            calendarSelectorProps={calendarSelectorProps}
            fullWidth
          />
          <BaseControlledTimePicker
            name='startTime'
            label='Start Time'
            control={control}
            iconName='clock'
            fullWidth
          />
        </div>
      </fieldset>
    </div>
  );
});
