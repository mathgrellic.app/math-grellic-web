import { memo } from 'react';
import { useFormContext } from 'react-hook-form';

import {
  BaseControlledInput,
  BaseControlledNumberInput,
} from '#/components/base/base-input.component';
import { BaseControlledRichTextEditor } from '#/components/base/base-rich-text-editor.component';

import type { LessonUpsertFormData } from '#/models/lesson.model';

export const LessonUpsertFormStep1 = memo(function LessonUpsertFormStep1() {
  const { control } = useFormContext<LessonUpsertFormData>();

  return (
    <div>
      <fieldset className='flex gap-5 flex-wrap'>
        <div className='flex justify-between items-start gap-5 w-full'>
          <BaseControlledInput
            label='Title'
            name='title'
            control={control}
            fullWidth
            asterisk
          />
          <BaseControlledNumberInput
            label='Lesson No.'
            name='orderNumber'
            control={control}
            fullWidth
            asterisk
          />
        </div>
        <div className='flex justify-between items-start gap-5 w-full'>
          <BaseControlledInput
            label='Video Url'
            name='videoUrl'
            control={control}
            fullWidth
            asterisk
          />
          <BaseControlledNumberInput
            label='Duration (in mins)'
            name='durationSeconds'
            control={control}
            fullWidth
          />
        </div>
        <BaseControlledRichTextEditor
          label='Lesson Description'
          name='description'
          control={control}
        />
      </fieldset>
    </div>
  );
});
