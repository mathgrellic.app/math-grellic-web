import { memo, useMemo } from 'react';
import { cx } from 'classix';

import { BaseIcon } from '#/components/base/base-icon.component';
import { BaseDivider } from '#/components/base/base-divider.component';

import type { ComponentProps } from 'react';
import type { Lesson } from '#/models/lesson.model';

type Props = ComponentProps<'div'> & {
  lesson: Lesson;
};

export const LessonSingle = memo(function LessonSingle({
  className,
  lesson,
  ...moreProps
}: Props) {
  const orderNumber = useMemo(() => lesson.orderNumber, [lesson]);
  const durationSeconds = useMemo(() => lesson.durationSeconds || 0, [lesson]);

  return (
    <div className={cx('w-full', className)} {...moreProps}>
      <div className='flex justify-center items-center w-full'>
        <div className='flex items-center'>
          {/* Lesson Number */}
          <div className='flex items-center'>
            <BaseIcon name='chalkboard-teacher' />
            <span className='uppercase'>Lesson {orderNumber}</span>
          </div>
          <BaseDivider vertical />
          {/* Lesson Duration */}
          <div className='flex items-center'>
            <BaseIcon name='hourglass' />
            <span className='uppercase'>{durationSeconds} mins</span>
          </div>
        </div>
      </div>
    </div>
  );
});
