import { memo } from 'react';
import cx from 'classix';

import type { ComponentProps } from 'react';

type Props = ComponentProps<'div'> & { vertical?: boolean };

export const BaseDivider = memo(function BaseDivider({
  className,
  vertical,
  ...moreProps
}: Props) {
  return (
    <div
      className={cx(
        'border-accent/20',
        vertical ? 'w-px h-full border-r' : 'w-full h-px border-b',
        className,
      )}
      {...moreProps}
    />
  );
});
