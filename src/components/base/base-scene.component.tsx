'use client';

import { memo, useCallback } from 'react';
import { useRouter } from 'next/navigation';
import Breadcrumbs from '@marketsystems/nextjs13-appdir-breadcrumbs';
import cx from 'classix';

import { BaseControlButton } from './base-control-button.component';

import type { ComponentProps, ReactNode } from 'react';

type Props = ComponentProps<'div'> & {
  title?: string;
  toolbarHidden?: boolean;
  breadcrumbsHidden?: boolean;
  headerRightContent?: ReactNode;
  isClose?: boolean;
};

export const BaseScene = memo(function BaseScene({
  className,
  title,
  breadcrumbsHidden,
  headerRightContent,
  toolbarHidden,
  isClose,
  children,
  ...moreProps
}: Props) {
  const router = useRouter();

  const handleBack = useCallback(() => {
    router.back();
  }, [router]);

  const handleClose = useCallback(() => {
    window.close();
  }, []);

  return (
    <div className={cx('relative px-9 z-10', className)} {...moreProps}>
      {!!title?.trim().length && (
        <div className='flex justify-start items-center h-20'>
          <h1 className='text-2xl w-fit'>{title}</h1>
        </div>
      )}
      {!toolbarHidden && (
        <div className='flex justify-between items-center w-full min-h-[46px]'>
          <div className='flex justify-start items-center gap-2'>
            {isClose ? (
              <BaseControlButton leftIconName='x' onClick={handleClose}>
                Close
              </BaseControlButton>
            ) : (
              <BaseControlButton leftIconName='arrow-left' onClick={handleBack}>
                Back
              </BaseControlButton>
            )}
            {!breadcrumbsHidden && (
              <Breadcrumbs
                listClassName='flex items-center text-accent/80 text-sm'
                inactiveItemClassName='flex hover:text-primary after:content-["/"] after:mx-1.5'
                activeItemClassName='!pointer-events-none'
                omitRootLabel
              />
            )}
          </div>
          {!!headerRightContent && <div>{headerRightContent}</div>}
        </div>
      )}
      {children}
    </div>
  );
});
