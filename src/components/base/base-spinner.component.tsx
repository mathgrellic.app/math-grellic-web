import { ComponentProps, memo } from 'react';
import cx from 'classix';

export const BaseSpinner = memo(function BaseSpinner({
  className,
  ...moreProps
}: ComponentProps<'div'>) {
  return (
    <div
      className={cx(
        'animate-spin inline-block w-16 h-16 border-[6px] border-current border-t-primary-focus text-primary/50 rounded-full',
        className,
      )}
      role='status'
      aria-label='loading'
      {...moreProps}
    >
      <span className='sr-only'>Loading...</span>
    </div>
  );
});

export const BasePageSpinner = memo(function BasePageSpinner({
  className,
  ...moreProps
}: ComponentProps<'div'>) {
  return (
    <div
      className={cx(
        'flex justify-center items-center w-full min-h-screen animate-fadeIn',
        className,
      )}
      {...moreProps}
    >
      <BaseSpinner />
    </div>
  );
});
