'use client';

import { memo, useCallback, useEffect, useMemo, useState } from 'react';
import dayjs from 'dayjs';
import cx from 'classix';
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react';
import { LazyMotion, domAnimation, m } from 'framer-motion';

import { BaseButton } from './base-button.component';
import { BaseIconButton } from './base-icon-button.component';
import { BaseDropdownButton } from './base-dropdown-button.component';

import type { ComponentProps } from 'react';

type Props = Omit<ComponentProps<'div'>, 'onChange'> & {
  currentDate?: Date;
  minDate?: Date;
  maxDate?: Date;
  isExpanded?: boolean;
  onExpand?: (isExpand: boolean) => void;
  onChange?: (date: Date) => void;
};

const widthAnimation = {
  initial: { width: '0%' },
  animate: { width: '100%' },
};

export const BaseCalendarSelector = memo(function BaseCalendarSelector({
  className,
  currentDate = new Date(),
  minDate = new Date('01-01-1900'),
  maxDate = new Date(),
  isExpanded,
  onExpand,
  onChange,
  ...moreProps
}: Props) {
  const [currentYear, setCurrentYear] = useState<number | undefined>(undefined);

  const date = useMemo(
    () => dayjs(currentDate).format('MMMM YYYY'),
    [currentDate],
  );

  useEffect(() => {
    if (!isExpanded) {
      return;
    }

    const currentYearId = `year-${currentDate.getFullYear()}`;
    const element = document.getElementById(currentYearId);
    !!element && element.scrollIntoView({ block: 'nearest' });
  }, [isExpanded, currentDate]);

  // Generate years from min and max date
  const years = useMemo(() => {
    let list = [];
    let i = minDate.getFullYear();
    const maxYear = maxDate.getFullYear();

    while (i <= maxYear) {
      list.push(i);
      ++i;
    }
    return list;
  }, [minDate, maxDate]);

  const months = useMemo(() => {
    if (!currentYear) {
      return [];
    }

    const isMaxYear = maxDate.getFullYear() === currentYear;
    const maxMonthIndex = maxDate.getMonth();

    return dayjs.months().map((m, i) => ({
      value: i + 1,
      label: m,
      disabled: isMaxYear && i > maxMonthIndex,
    }));
  }, [currentYear, maxDate]);

  const changeCurrentDate = useCallback(
    (date: Date) => {
      // If selected month and year is over max date then cancel
      if (dayjs(date).isAfter(maxDate, 'month')) {
        return;
      }

      !!onChange && onChange(date);
      !!onExpand && onExpand(false);
    },
    [maxDate, onChange, onExpand],
  );

  const handleSetCurrentYear = useCallback(
    (year: number) => () => setCurrentYear(year),
    [],
  );

  const handleExpand = useCallback(
    () => !!onExpand && onExpand(!isExpanded),
    [isExpanded, onExpand],
  );

  const handleChange = useCallback(
    (isNext: boolean) => () => {
      // Add or subtract a month from current date
      const date = isNext
        ? dayjs(currentDate).add(1, 'month')
        : dayjs(currentDate).subtract(1, 'month');

      changeCurrentDate(date.toDate());
    },
    [currentDate, changeCurrentDate],
  );

  const handleSpecificChange = useCallback(
    (month: number) => () => {
      const date = dayjs(`${month}-01-${currentYear}`).toDate();
      changeCurrentDate(date);
      setCurrentYear(undefined);
    },
    [currentYear, changeCurrentDate],
  );

  return (
    <div className={cx('flex flex-col w-full', className)} {...moreProps}>
      <div className='flex justify-between items-center w-full px-2.5'>
        <BaseButton
          className='!font-body !text-base !tracking-normal'
          variant='link'
          size='sm'
          leftIconName='calendar'
          onClick={handleExpand}
        >
          {date}
        </BaseButton>
        <div className='flex items-center w-fit'>
          <BaseIconButton
            name='caret-circle-left'
            variant='link'
            className='w-9'
            onClick={handleChange(false)}
          />
          <BaseIconButton
            name='caret-circle-right'
            variant='link'
            className='w-9'
            onClick={handleChange(true)}
          />
        </div>
      </div>
      {isExpanded && (
        <div className='flex items-start w-full h-[302px] border-t border-t-primary-border-light overflow-hidden'>
          <div className='w-full h-full'>
            <OverlayScrollbarsComponent className='w-full h-full p-2.5' defer>
              {years.map((y) => (
                <BaseDropdownButton
                  key={y}
                  type='button'
                  id={`year-${y}`}
                  className='items-center'
                  selected={y === currentYear}
                  onClick={handleSetCurrentYear(y)}
                  center
                >
                  {y}
                </BaseDropdownButton>
              ))}
            </OverlayScrollbarsComponent>
          </div>
          {currentYear !== undefined && (
            <LazyMotion features={domAnimation}>
              <m.div className='w-full h-full' {...widthAnimation}>
                <OverlayScrollbarsComponent
                  className='w-full h-full p-2.5 border-l border-l-primary-border-light'
                  defer
                >
                  {months.map(({ label, value, disabled }) => (
                    <BaseDropdownButton
                      key={value}
                      type='button'
                      className='items-center'
                      disabled={disabled}
                      onClick={handleSpecificChange(value)}
                      center
                    >
                      {label}
                    </BaseDropdownButton>
                  ))}
                </OverlayScrollbarsComponent>
              </m.div>
            </LazyMotion>
          )}
        </div>
      )}
    </div>
  );
});
