'use client';

import { forwardRef, memo } from 'react';
import cx from 'classix';

import { BaseIcon } from './base-icon.component';

import type { ComponentProps } from 'react';
import type { ButtonSize, ButtonVariant, IconName } from '#/models/base.model';

type Props = ComponentProps<'button'> & {
  variant?: ButtonVariant;
  size?: ButtonSize;
  leftIconName?: IconName;
  rightIconName?: IconName;
  bodyFont?: boolean;
  loading?: boolean;
};

type IconProps = ComponentProps<typeof BaseIcon> & {
  variant: ButtonVariant;
  size: ButtonSize;
  name: IconName;
};

const Icon = memo(function Icon({
  variant,
  size,
  name,
  ...moreProps
}: IconProps) {
  switch (variant) {
    case 'primary':
      return (
        <BaseIcon
          className='px-2.5 box-content'
          size={24}
          weight='regular'
          {...moreProps}
          name={name}
        />
      );
    case 'link':
      const iconSize = size === 'sm' || size === 'xs' ? 24 : 28;
      return (
        <BaseIcon size={iconSize} weight='regular' {...moreProps} name={name} />
      );
    case 'border':
      return <BaseIcon size={22} weight='regular' {...moreProps} name={name} />;
    default:
      return <BaseIcon size={20} weight='bold' {...moreProps} name={name} />;
  }
});

export const BaseButton = memo(
  forwardRef<HTMLButtonElement, Props>(function BaseButton(
    {
      className,
      variant = 'solid',
      size = 'base',
      leftIconName,
      rightIconName,
      bodyFont,
      loading,
      disabled,
      children,
      ...moreProps
    },
    ref,
  ) {
    return (
      <button
        ref={ref}
        type='button'
        className={cx(
          'button inline-flex justify-center items-center w-fit font-display tracking-tighter !outline-none transition-all active:scale-95',
          variant === 'primary' &&
            'group relative !items-start h-[55px] text-white text-lg drop-shadow-primary-lg',
          (variant === 'solid' || variant === 'border') &&
            'px-6 py-2 h-12 border-2 rounded-md',
          variant === 'solid' &&
            'bg-primary text-white border-primary-border hover:bg-primary-focus drop-shadow-primary',
          variant === 'solid' &&
            disabled &&
            '!bg-gray-300 !border-accent/40 !text-accent/50',
          variant === 'border' &&
            'border-primary text-primary hover:text-primary-focus-light hover:border-primary-focus-light !transition-transform',
          variant === 'border' &&
            disabled &&
            '!border-accent/40 !text-accent/50',
          variant === 'link' &&
            'py-0.5 text-primary text-lg hover:text-primary-focus-light !transition-transform',
          variant === 'link' && disabled && '!text-accent/50',
          (variant === 'solid' || variant === 'border') &&
            size === 'sm' &&
            '!h-10',
          (variant === 'solid' || variant === 'border') &&
            size === 'xs' &&
            '!h-9 !px-3 !text-sm',
          variant === 'link' && (size === 'sm' || size === 'xs') && '!text-sm',
          bodyFont && '!font-body !tracking-normal',
          disabled && '!pointer-events-none',
          className,
        )}
        disabled={disabled}
        {...moreProps}
      >
        <div
          className={cx(
            'inline-flex justify-center items-center gap-2 transition-all',
            variant === 'primary' &&
              'group-hover:brightness-125 group-active:translate-y-[5px] relative h-[50px] bg-gradient-to-r from-primary-focus to-primary-dark border-2 border-primary-border rounded-full z-10',
            variant === 'primary' &&
              disabled &&
              '!bg-none !bg-gray-300 !border-accent/40',
          )}
        >
          {leftIconName &&
            (variant === 'primary' ? (
              <div className='relative flex items-center h-full'>
                <div className='absolute right-0 top-0 h-full w-px bg-backdrop opacity-30 scale-y-75' />
                <Icon name={leftIconName} variant={variant} size={size} />
              </div>
            ) : (
              <Icon name={leftIconName} variant={variant} size={size} />
            ))}
          {variant === 'primary' ? (
            <div
              className={cx(
                'px-[28px]',
                rightIconName && '!pr-4',
                disabled && '!text-accent/50',
              )}
            >
              {children}
            </div>
          ) : (
            children
          )}
          {rightIconName &&
            (variant === 'primary' ? (
              <div className='relative flex items-center h-full'>
                <div className='absolute left-0 top-0 h-full w-px bg-backdrop opacity-30 scale-y-75' />
                <Icon name={rightIconName} variant={variant} size={size} />
              </div>
            ) : (
              <Icon name={rightIconName} variant={variant} size={size} />
            ))}
        </div>
        {variant === 'primary' && (
          <div
            className={cx(
              'absolute bottom-0 w-full h-[50px] border-2 border-primary-border rounded-full bg-black',
              disabled && '!border-accent/40 !bg-backdrop-gray',
            )}
          >
            <div
              className={cx(
                'group-hover:brightness-125 w-full h-full bg-gradient-to-r from-primary-focus to-primary-dark rounded-full opacity-90',
                disabled && '!bg-none !bg-gray-300',
              )}
            />
          </div>
        )}
      </button>
    );
  }),
);
