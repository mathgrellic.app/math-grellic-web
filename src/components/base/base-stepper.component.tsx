'use client';

import {
  Children,
  isValidElement,
  memo,
  useCallback,
  useMemo,
  useState,
} from 'react';
import { AnimatePresence, LazyMotion, domAnimation, m } from 'framer-motion';
import cx from 'classix';

import {
  stepperAnimationTransition,
  stepperAnimationVariants,
} from '#/utils/animations.util';
import { BaseDivider } from './base-divider.component';
import { BaseIcon } from './base-icon.component';
import { BaseStepperControls } from './base-stepper-controls.component';
import { BaseStepperStep } from './base-stepper-step.component';

import type { ComponentProps, ReactNode } from 'react';

type StepperControlsProps = ComponentProps<typeof BaseStepperControls>;

type Props = ComponentProps<'div'> & {
  controlsRightContent?: ReactNode;
  onReset?: StepperControlsProps['onReset'];
  onPrev?: StepperControlsProps['onPrev'];
  onNext?: StepperControlsProps['onNext'];
};

export const BaseStepper = memo(function BaseStepper({
  className,
  controlsRightContent,
  onReset,
  onPrev,
  onNext,
  children,
  ...moreProps
}: Props) {
  const [currentIndex, setCurrentIndex] = useState(0);
  const [prevIndex, setPrevIndex] = useState(currentIndex);
  const [isAnimating, setIsAnimating] = useState(false);

  const steps = useMemo(
    () =>
      Children.map(children, (child) => {
        const isValid =
          isValidElement(child) &&
          (child as any).type.displayName === 'BaseStepperStep';

        return isValid ? child : null;
      })?.filter((child) => !!child),
    [children],
  );

  const stepLabels = useMemo(
    () =>
      steps?.map(
        (c) => (c.props as ComponentProps<typeof BaseStepperStep>).label,
      ) || [],
    [steps],
  );

  const isForward = useMemo(
    () => (prevIndex <= currentIndex ? 1 : 0),
    [prevIndex, currentIndex],
  );

  const handleNextStep = useCallback(() => {
    if (isAnimating || !steps || currentIndex >= steps.length - 1) {
      return;
    }

    !!onNext && onNext();
    setPrevIndex(currentIndex);
    setCurrentIndex(currentIndex + 1);
  }, [isAnimating, steps, currentIndex, onNext]);

  const handlePrevStep = useCallback(() => {
    if (isAnimating || currentIndex <= 0) {
      return;
    }

    !!onPrev && onPrev();
    setPrevIndex(currentIndex);
    setCurrentIndex(currentIndex - 1);
  }, [isAnimating, currentIndex, onPrev]);

  const handleReset = useCallback(() => {
    onReset && onReset();
    setCurrentIndex(0);
  }, [onReset]);

  const handleAnimation = useCallback(
    (isAnimating: boolean) => () => setIsAnimating(isAnimating),
    [],
  );

  return (
    <div className={cx('w-full', className)} {...moreProps}>
      <div className='flex justify-center items-center w-full'>
        <BaseDivider />
        <ol className='mx-4 flex items-center'>
          {stepLabels.map((label, index) => (
            <li
              key={index}
              className={cx(
                "py-2 flex items-center after:content-[''] after:mx-2.5 after:w-11 after:h-1 after:border-b after:border-accent last:after:content-none",
                index <= currentIndex && 'text-primary-focus font-medium',
                index < currentIndex && 'after:border-primary-focus',
              )}
            >
              <div className='flex items-center justify-center w-5 h-5 mr-1'>
                {index < currentIndex ? (
                  <BaseIcon name='check-circle' size={20} weight='fill' />
                ) : (
                  <span>{index + 1}</span>
                )}
              </div>
              <span className='w-max'>{label}</span>
            </li>
          ))}
        </ol>
        <BaseDivider />
      </div>
      <BaseStepperControls
        onPrev={handlePrevStep}
        onNext={handleNextStep}
        onReset={handleReset}
      >
        {controlsRightContent}
      </BaseStepperControls>
      <div className='relative py-5 mx-auto max-w-[600px] w-full !overflow-hidden min-h-[700px]'>
        {!!steps && (
          <LazyMotion features={domAnimation}>
            <AnimatePresence
              initial={false}
              custom={isForward}
              mode='popLayout'
            >
              <m.div
                key={currentIndex}
                className='w-full h-full'
                custom={isForward}
                variants={stepperAnimationVariants}
                initial='initial'
                animate='animate'
                exit='exit'
                transition={stepperAnimationTransition}
                onAnimationStart={handleAnimation(true)}
                onAnimationComplete={handleAnimation(false)}
              >
                {steps[currentIndex]}
              </m.div>
            </AnimatePresence>
          </LazyMotion>
        )}
      </div>
    </div>
  );
});
