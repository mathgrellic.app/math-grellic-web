'use client';

import {
  Fragment,
  forwardRef,
  memo,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { Listbox, Transition } from '@headlessui/react';
import { useController } from 'react-hook-form';
import cx from 'classix';

import { menuAnimation } from '#/utils/animations.util';
import { BaseIcon } from './base-icon.component';
import { BaseSurface } from './base-surface.component';
import { BaseDropdownButton } from './base-dropdown-button.component';

import type { ComponentProps } from 'react';
import type {
  ControllerRenderProps,
  UseControllerProps,
} from 'react-hook-form';
import type { IconName, SelectOption } from '#/models/base.model';

type Props = Omit<ComponentProps<typeof Listbox>, 'onChange'> & {
  options: SelectOption[];
  optionSize?: 'base' | 'sm';
  name?: string;
  value?: string;
  label?: string;
  description?: string;
  errorMessage?: string;
  iconName?: IconName;
  fullWidth?: boolean;
  asterisk?: boolean;
  required?: boolean;
  buttonProps?: ComponentProps<typeof Listbox.Button>;
  onChange?: ControllerRenderProps['onChange'];
};

export const BaseSelect = memo(
  forwardRef<HTMLDivElement, Props>(function BaseSelect(
    {
      name,
      id,
      className,
      options,
      optionSize = 'base',
      value,
      label,
      description,
      errorMessage,
      iconName,
      fullWidth,
      asterisk,
      required,
      disabled,
      buttonProps: { className: buttonClassName, ...moreButtonProps } = {},
      onChange,
      ...moreProps
    },
    ref,
  ) {
    const [localValue, setLocalValue] = useState<string | undefined>(value);
    const currentLabel = useMemo(
      () => options.find((option) => option.value == localValue)?.label,
      [localValue, options],
    );

    const handleChange = useCallback(
      (value: string) => {
        setLocalValue(value);
        !!onChange && onChange(value);
      },
      [onChange],
    );

    useEffect(() => {
      if (!!value) {
        return;
      }

      setLocalValue(value);
    }, [value]);

    return (
      <Listbox
        as='div'
        ref={ref}
        className={cx(
          'relative w-full',
          !fullWidth && 'max-w-input',
          className,
        )}
        onChange={handleChange}
        {...moreProps}
      >
        <Listbox.Button
          className={cx(
            `group mb-0.5 pl-18px pr-4 w-full h-input border-2 border-accent/40 text-left flex items-center
            focus:!border-primary-focus focus:!ring-1 focus:!ring-primary-focus bg-white text-accent rounded-md !outline-none transition-all`,
            !!iconName && '!pl-[13px]',
            !!errorMessage && '!border-red-500/60',
            disabled && '!bg-backdrop-gray !pointer-events-none',
            buttonClassName,
          )}
          {...moreButtonProps}
        >
          <div
            className={cx(
              'relative pt-26px pb-2 w-full h-full',
              !!iconName && 'pl-[31px]',
            )}
          >
            {!!iconName && (
              <BaseIcon
                name={iconName}
                size={22}
                className={cx(
                  'absolute left-0 top-1/2 -translate-y-1/2 group-focus:!text-primary',
                  !!errorMessage && '!text-red-500',
                )}
              />
            )}
            {!!label && (
              <span
                className={cx(
                  `absolute left-0 top-1/2 -translate-y-1/2 font-bold text-accent/70 group-focus:!text-primary transition-all`,
                  localValue !== undefined &&
                    '!-translate-y-111 !text-13px after:!top-0',
                  !!iconName && '!left-[31px]',
                  !!errorMessage && '!text-red-500',
                  (asterisk || required) &&
                    "after:content-['*'] after:absolute after:top-0.5 after:pl-1.5 after:text-xl after:text-yellow-500",
                )}
              >
                {label}
              </span>
            )}
            {currentLabel}
          </div>
          <BaseIcon
            name='caret-down'
            size={22}
            weight='fill'
            className='shrink-0 group-hover:!text-primary group-focus:!text-primary'
          />
        </Listbox.Button>
        {!!description && !errorMessage && (
          <small className='inline-block px-1 text-accent/80'>
            {description}
          </small>
        )}
        {!!errorMessage && (
          <small className='inline-block px-1 text-red-500'>
            {errorMessage}
          </small>
        )}
        <Transition as={Fragment} {...menuAnimation}>
          <Listbox.Options
            as={BaseSurface}
            className='absolute !p-1.5 w-full top-full left-0 mt-2.5 z-50 drop-shadow-primary-sm'
            rounded='xs'
          >
            {options.map(
              ({ label: opLabel, value: opValue, iconName: opIconName }) => (
                <Listbox.Option
                  as={BaseDropdownButton}
                  key={opValue}
                  value={opValue}
                  checked={localValue === opValue}
                  size={optionSize}
                  iconName={opIconName}
                >
                  {opLabel}
                </Listbox.Option>
              ),
            )}
          </Listbox.Options>
        </Transition>
      </Listbox>
    );
  }),
);

export function BaseControlledSelect(props: Props & UseControllerProps<any>) {
  const {
    field,
    fieldState: { error },
  } = useController(props);

  return <BaseSelect {...props} {...field} errorMessage={error?.message} />;
}
