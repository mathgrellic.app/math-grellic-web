import { Fragment } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import cx from 'classix';

import {
  modalAnimationBackdrop,
  modalAnimationPanel,
} from '#/utils/animations.util';
import { BaseSurface } from './base-surface.component';
import { BaseControlButton } from './base-control-button.component';

import type { ComponentProps, ReactNode } from 'react';
import type { ModalSize } from '#/models/base.model';

type Props = Omit<ComponentProps<typeof BaseSurface>, 'children'> & {
  open: boolean;
  onClose: () => void;
  size?: ModalSize;
  children?: ReactNode;
};

export function BaseModal({
  className,
  open,
  size = 'base',
  children,
  onClose = () => null,
  ...moreProps
}: Props) {
  return (
    <Transition appear show={open} as={Fragment}>
      <Dialog as='div' onClose={onClose}>
        <Transition.Child as={Fragment} {...modalAnimationBackdrop}>
          <div className='fixed inset-0 backdrop-blur-lg bg-black/20 z-max' />
        </Transition.Child>
        <div className='fixed inset-0 overflow-y-auto z-max'>
          <Transition.Child as={Fragment} {...modalAnimationPanel}>
            <div className='flex min-h-full items-center justify-center'>
              <Dialog.Panel
                className={cx(
                  'max-w-[700px] w-full transition-all',
                  size === 'sm' && '!max-w-xl',
                  size === 'lg' && '!max-w-[968px]',
                  size === 'none' && '!max-w-none',
                )}
              >
                <BaseSurface
                  className={cx(
                    'pt-14 relative w-full min-h-[150px] !bg-backdrop shadow-md overflow-hidden',
                    className,
                  )}
                  rounded='lg'
                  {...moreProps}
                >
                  {!!onClose && (
                    <BaseControlButton
                      className='absolute right-5 top-5'
                      leftIconName='x'
                      onClick={onClose}
                    >
                      Close
                    </BaseControlButton>
                  )}
                  {children}
                </BaseSurface>
              </Dialog.Panel>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition>
  );
}
