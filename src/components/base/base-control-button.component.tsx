import { forwardRef, memo } from 'react';
import cx from 'classix';

import { BaseIcon } from './base-icon.component';

import type { ComponentProps } from 'react';
import type { IconWeight } from '@phosphor-icons/react';
import type { IconName } from '#/models/base.model';

type Props = ComponentProps<'button'> & {
  leftIconName?: IconName;
  rightIconName?: IconName;
  disabled?: boolean;
};

const iconProps = {
  size: 15,
  weight: 'bold' as IconWeight,
  className: 'mt-[1px]',
};

export const BaseControlButton = memo(
  forwardRef<HTMLButtonElement, Props>(function BaseControlButton(
    {
      className,
      leftIconName,
      rightIconName,
      disabled,
      children,
      ...moreProps
    },
    ref,
  ) {
    return (
      <button
        ref={ref}
        type='button'
        className={cx(
          `py-1 px-2 inline-flex justify-center items-center gap-2 !outline-none text-primary hover:text-primary-focus-light
            text-sm font-medium uppercase tracking-widest leading-none transition-colors`,
          disabled && '!text-accent/50 !pointer-events-none bg-gray-300',
          className,
        )}
        disabled={disabled}
        {...moreProps}
      >
        {leftIconName && <BaseIcon name={leftIconName} {...iconProps} />}
        {children}
        {rightIconName && <BaseIcon name={rightIconName} {...iconProps} />}
      </button>
    );
  }),
);
