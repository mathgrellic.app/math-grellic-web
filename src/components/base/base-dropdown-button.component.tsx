import { memo, forwardRef } from 'react';
import cx from 'classix';

import { BaseIcon } from './base-icon.component';

import type { ComponentProps } from 'react';
import type { IconName } from '#/models/base.model';

type Props = ComponentProps<'button'> & {
  size?: 'base' | 'sm';
  selected?: boolean;
  checked?: boolean;
  center?: boolean;
  iconName?: IconName;
};

export const BaseDropdownButton = memo(
  forwardRef<HTMLButtonElement, Props>(function BaseDropdownButton(
    {
      className,
      size = 'base',
      iconName,
      selected,
      checked,
      center,
      disabled,
      children,
      ...moreProps
    },
    ref,
  ) {
    return (
      <button
        ref={ref}
        type='button'
        className={cx(
          'flex flex-col px-3 py-2.5 w-full text-left leading-none transition-colors rounded',
          size === 'base' ? 'text-base' : 'text-sm',
          !disabled
            ? 'hover:bg-primary hover:text-white'
            : 'text-accent/50 !pointer-events-none',
          selected && '!bg-primary !text-white',
          className,
        )}
        disabled={disabled}
        {...moreProps}
      >
        <div
          className={cx(
            'relative flex items-center w-full h-4',
            center ? 'justify-center' : 'justify-between',
          )}
        >
          <div className='flex items-center gap-x-2 h-full'>
            {!!iconName && (
              <BaseIcon name={iconName} size={size === 'base' ? 18 : 20} />
            )}
            {children}
          </div>
          {!!checked && (
            <BaseIcon
              name='check-fat'
              className='text-green-500'
              size={size === 'base' ? 18 : 20}
              weight='fill'
            />
          )}
        </div>
      </button>
    );
  }),
);
