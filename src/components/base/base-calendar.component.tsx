import { memo, useCallback, useMemo } from 'react';
import dayjs from 'dayjs';
import cx from 'classix';

import type { ComponentProps } from 'react';
import type { Dayjs } from 'dayjs';

type Props = Omit<ComponentProps<'div'>, 'onChange'> & {
  currentDate: Date;
  selectedDate?: string;
  deteRowClassName?: string;
  onChange?: (date: Date) => void;
};

export const BaseCalendar = memo(function BaseCalendar({
  className,
  currentDate = new Date(),
  selectedDate,
  deteRowClassName,
  onChange,
  ...moreProps
}: Props) {
  const rows = useMemo(() => {
    const cells = getCalendarCells(dayjs(currentDate));
    let rows = [];

    // Split one array into chunks
    for (let i = 0; i < cells.length; i += 7) {
      rows.push(cells.slice(i, i + 7));
    }

    return rows;
  }, [currentDate]);

  const handleDateChange = useCallback(
    (date: Dayjs) => () => !!onChange && onChange(date.toDate()),
    [onChange],
  );

  return (
    <div className={cx('flex flex-col w-full', className)} {...moreProps}>
      <div className='flex items-center w-full'>
        {rows[0].map(({ value }, i) => (
          <div
            key={i}
            className='flex justify-center items-center w-full h-10 text-sm border-r border-primary-border-light last:border-transparent'
          >
            {value.format('dd')}
          </div>
        ))}
      </div>
      <div className='flex flex-col flex-1 w-full'>
        {rows.map((cells, rowIndex) => (
          <div key={rowIndex} className='group flex flex-1 items-center w-full'>
            {cells.map(({ text, value, isPresent }, i) => (
              <button
                key={`${value}-${i}`}
                type='button'
                className={cx(
                  `p-[3px] w-full h-full border border-transparent border-b-primary-border-light border-r-primary-border-light last:border-r-transparent group-first:border-t-primary-border-light
                    group-last:border-b-transparent text-sm hover:text-primary-focus-light hover:!border-primary-focus-light hover:bg-backdrop-light active:scale-90 transition-transform`,
                  !isPresent && 'text-accent/70',
                  !!selectedDate &&
                    value.isSame(dayjs(selectedDate), 'day') &&
                    '!border-primary !bg-primary !rounded-sm',
                )}
                onClick={handleDateChange(value)}
              >
                <div
                  className={cx(
                    'flex justify-center items-center w-full h-full',
                    !!selectedDate &&
                      value.isSame(dayjs(selectedDate), 'day') &&
                      'rounded-md bg-backdrop-light',
                  )}
                >
                  {text}
                </div>
              </button>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
});

function prepareCell(date: Dayjs, dayNum: number, isPresent?: boolean) {
  return {
    text: dayNum.toString().padStart(2, '0'),
    value: date.clone().set('date', dayNum),
    isPresent,
  };
}

function getCalendarCells(date: Dayjs) {
  let calendarCells = [];
  const daysInMonth = date.daysInMonth();
  const prevMonth = date.subtract(1, 'month');
  const nextMonth = date.add(1, 'month');

  // Push previous month day cells
  const startDayIndex = date.startOf('month').day();
  for (let i = startDayIndex - 1; i >= 1; i--) {
    calendarCells.push(
      prepareCell(prevMonth, prevMonth.daysInMonth() - (i - 1)),
    );
  }

  // Push current month day cells
  for (let i = 0; i < daysInMonth; i++) {
    calendarCells.push(prepareCell(date, i + 1, true));
  }

  // Push next month day cells
  const cellsLeft = 35 - calendarCells.length;
  const cellsNeeded = cellsLeft > 0 ? cellsLeft : 7 - Math.abs(cellsLeft);
  for (let i = 0; i < cellsNeeded; i++) {
    calendarCells.push(prepareCell(nextMonth, i + 1));
  }

  return calendarCells;
}
