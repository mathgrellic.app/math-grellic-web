'use client';

import { memo, forwardRef, useState, useCallback, useMemo } from 'react';
import { useController } from 'react-hook-form';
import { PatternFormat } from 'react-number-format';
import cx from 'classix';

import { BaseIcon } from './base-icon.component';
import { BaseIconButton } from './base-icon-button.component';

import type { ComponentProps, FormEvent } from 'react';
import type { UseControllerProps } from 'react-hook-form';
import type { IconName } from '#/models/base.model';

type Props = ComponentProps<'input'> & {
  label?: string;
  description?: string;
  errorMessage?: string;
  iconName?: IconName;
  fullWidth?: boolean;
  asterisk?: boolean;
  wrapperProps?: ComponentProps<'div'>;
  rightButtonProps?: ComponentProps<typeof BaseIconButton>;
};

type ControlledProps = Props & UseControllerProps<any>;

export const BaseInput = memo(
  forwardRef<HTMLInputElement, Props>(function BaseInput(
    {
      className,
      id,
      name,
      value,
      placeholder = '',
      label,
      description,
      errorMessage,
      iconName,
      fullWidth,
      asterisk,
      required,
      disabled,
      wrapperProps: { className: wrapperClassName, ...moreWrapperProps } = {},
      rightButtonProps: {
        className: rightIconBtnClassName,
        name: rightIconBtnName,
        ...moreRightIconBtnProps
      } = {},
      ...moreProps
    },
    ref,
  ) {
    const newId = id || name;

    return (
      <div
        className={cx(
          'base-input w-full',
          !fullWidth && 'max-w-input',
          wrapperClassName,
        )}
        {...moreWrapperProps}
      >
        <div className='relative mb-0.5 w-full h-input'>
          <input
            ref={ref}
            name={name}
            type='text'
            id={newId}
            className={cx(
              `peer pl-18px pr-5 pt-26px pb-2 w-full h-full border-2 border-accent/40 focus:!border-primary-focus focus:!ring-1 focus:!ring-primary-focus
                text-accent rounded-md !outline-none transition-all`,
              !!iconName && '!pl-43px',
              !!errorMessage && '!border-red-500/60',
              !!rightIconBtnName && '!pr-11',
              className,
              disabled && '!bg-backdrop-gray',
            )}
            value={value}
            placeholder={placeholder}
            required={required}
            disabled={disabled}
            {...moreProps}
          />
          {!!iconName && (
            <BaseIcon
              name={iconName}
              size={22}
              className={cx(
                'absolute left-15px top-1/2 -translate-y-1/2 peer-focus:!text-primary',
                !!errorMessage && '!text-red-500',
              )}
            />
          )}
          {!!label && (
            <label
              htmlFor={newId}
              className={cx(
                `absolute left-5 top-1/2 -translate-y-1/2 peer-focus:-translate-y-111 peer-focus:text-13px font-bold text-accent/70 peer-focus:!text-primary
                  after:peer-focus:!top-0 transition-all`,
                !!value && '!-translate-y-111 !text-13px after:!top-0',
                !!iconName && '!left-45px',
                !!errorMessage && '!text-red-500',
                (asterisk || required) &&
                  "after:content-['*'] after:absolute after:top-0.5 after:pl-1.5 after:text-xl after:text-yellow-500",
              )}
            >
              {label}
            </label>
          )}
          {!!rightIconBtnName && (
            <BaseIconButton
              name={rightIconBtnName}
              variant='link'
              size='xs'
              className={cx(
                'absolute top-1/2 -translate-y-1/2 right-1 !text-accent hover:!text-primary',
                rightIconBtnClassName,
              )}
              disabled={disabled}
              {...moreRightIconBtnProps}
            />
          )}
        </div>
        {!!description && !errorMessage && (
          <small className='inline-block px-1 text-accent/80'>
            {description}
          </small>
        )}
        {!!errorMessage && (
          <small className='inline-block px-1 text-red-500'>
            {errorMessage}
          </small>
        )}
      </div>
    );
  }),
);

export function BaseControlledInput(props: ControlledProps) {
  const {
    field: { value, ...moreFields },
    fieldState: { error },
  } = useController(props);

  return (
    <BaseInput
      {...props}
      {...moreFields}
      value={value || ''}
      errorMessage={error?.message}
    />
  );
}

export function BaseControlledNumberInput(props: ControlledProps) {
  const {
    field: { value, onChange, ...moreFields },
    fieldState: { error },
  } = useController(props);

  const transformedValue = useMemo(() => {
    if (value == null) {
      return '';
    }
    return value.toString();
  }, [value]);

  const handleChange = useCallback(
    (event: FormEvent<HTMLInputElement>) => {
      if (event.currentTarget.value.trim() == '') {
        onChange('');
        return;
      }

      const output = isNaN(+event.currentTarget.value)
        ? event.currentTarget.value
        : +event.currentTarget.value;
      onChange(output);
    },
    [onChange],
  );

  return (
    <BaseInput
      {...props}
      {...moreFields}
      value={transformedValue}
      onChange={handleChange}
      errorMessage={error?.message}
    />
  );
}

export function BaseControlledPhoneInput(
  props: Props & UseControllerProps<any>,
) {
  const {
    field: { ref, onBlur, ...moreField },
    fieldState: { error },
  } = useController(props);

  const [isFocus, setIsFocus] = useState(false);

  const handleFocus = useCallback(() => setIsFocus(true), []);

  const handleBlur = useCallback(() => {
    onBlur();
    setIsFocus(false);
  }, [onBlur]);

  return (
    <PatternFormat
      {...props}
      {...moreField}
      type='text'
      customInput={BaseInput}
      errorMessage={error?.message}
      format='0###-###-####'
      mask='_'
      allowEmptyFormatting={isFocus}
      onFocus={handleFocus}
      onBlur={handleBlur}
    />
  );
}
