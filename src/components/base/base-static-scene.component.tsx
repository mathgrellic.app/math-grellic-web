import { memo } from 'react';
import cx from 'classix';

import type { ComponentProps } from 'react';

type Props = ComponentProps<'div'> & {
  title?: string;
};

export const BaseStaticScene = memo(function BaseStaticScene({
  className,
  title,
  children,
  ...moreProps
}: Props) {
  return (
    <div
      className={cx('relative pt-20 pb-36 z-10 animate-pageChange', className)}
      {...moreProps}
    >
      {!!title?.trim().length && (
        <h1 className='mb-32 pt-2 text-center'>{title}</h1>
      )}
      {children}
    </div>
  );
});
