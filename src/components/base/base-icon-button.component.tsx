'use client';

import { forwardRef, memo } from 'react';
import cx from 'classix';

import { BaseIcon } from './base-icon.component';

import type { ComponentProps } from 'react';
import type { ButtonSize, ButtonVariant, IconName } from '#/models/base.model';

type Props = ComponentProps<'button'> & {
  name: IconName;
  variant?: ButtonVariant;
  size?: ButtonSize;
  loading?: boolean;
  isInput?: boolean;
  iconProps?: Omit<ComponentProps<typeof BaseIcon>, 'name'>;
};

export const BaseIconButton = memo(
  forwardRef<HTMLButtonElement, Props>(function BaseIconButton(
    {
      className,
      name,
      variant = 'primary',
      size = 'base',
      loading,
      isInput,
      disabled,
      iconProps,
      ...moreProps
    },
    ref,
  ) {
    return (
      <button
        ref={ref}
        type='button'
        className={cx(
          'w-12 h-12 inline-flex justify-center items-center rounded-md !outline-none transition-all active:scale-90',
          variant === 'primary' &&
            'border-2 border-primary-border bg-primary text-white hover:bg-primary-focus drop-shadow-primary',
          variant === 'solid' &&
            'border border-primary-border-light hover:border-primary-focus-light bg-white text-primary-focus hover:text-primary-focus-light',
          variant === 'link' &&
            'border border-transparent text-primary hover:text-primary-focus-light !rounded',
          variant === 'border' &&
            'border-2 border-primary text-primary hover:text-primary-focus-light hover:border-primary-focus-light',
          size === 'sm' && '!w-10 !h-10',
          size === 'xs' && '!w-9 !h-9',
          isInput &&
            'p-2.5 !text-accent hover:!text-primary-focus !w-auto !h-auto',
          disabled && '!pointer-events-none',
          className,
        )}
        disabled={disabled}
        {...moreProps}
      >
        <BaseIcon
          size={size === 'base' ? 28 : 22}
          weight='regular'
          name={name}
          {...iconProps}
        />
      </button>
    );
  }),
);
