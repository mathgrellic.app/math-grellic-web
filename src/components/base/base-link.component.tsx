import { forwardRef, memo } from 'react';
import Link from 'next/link';
import cx from 'classix';

import { BaseIcon } from './base-icon.component';

import type { ComponentProps } from 'react';
import type { ButtonVariant, IconName } from '#/models/base.model';

type Variant = Omit<ButtonVariant, 'primary' | 'border'>;

type Props = ComponentProps<typeof Link> & {
  variant?: Variant;
  size?: 'base' | 'sm';
  leftIconName?: IconName;
  rightIconName?: IconName;
};

export const BaseLink = memo(
  forwardRef<HTMLAnchorElement, Props>(function BaseLink(
    {
      className,
      variant = 'link',
      size = 'base',
      leftIconName,
      rightIconName,
      children,
      ...moreProps
    },
    ref,
  ) {
    const iconSize = size === 'sm' ? 24 : 30;

    return (
      <Link
        ref={ref}
        className={cx(
          'py-0.5 gap-2 inline-flex justify-center items-center font-display tracking-tighter text-primary text-lg hover:text-primary-focus-light transition-colors',
          variant === 'solid' &&
            'solid px-5 h-[46px] bg-white border border-primary-border-light rounded-md hover:!border-primary-focus-light',
          size === 'sm' && '!text-base',
          className,
        )}
        {...moreProps}
      >
        {leftIconName && (
          <BaseIcon size={iconSize} weight='regular' name={leftIconName} />
        )}
        {children}
        {rightIconName && (
          <BaseIcon size={iconSize} weight='regular' name={rightIconName} />
        )}
      </Link>
    );
  }),
);
