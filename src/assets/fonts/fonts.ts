import localFont from 'next/font/local';
import type { NextFontWithVariable } from 'next/dist/compiled/@next/font';

export const bodyFont: NextFontWithVariable = localFont({
  variable: '--font-body',
  display: 'swap',
  src: [
    {
      path: '/body-400.woff2',
      weight: '400',
      style: 'normal',
    },
    {
      path: './body-500.woff2',
      weight: '500',
      style: 'normal',
    },
    {
      path: './body-500.woff2',
      weight: '600',
      style: 'normal',
    },
  ],
});

export const displayFont: NextFontWithVariable = localFont({
  variable: '--font-display',
  display: 'swap',
  src: [
    {
      path: './display-400.woff2',
      weight: '400',
      style: 'normal',
    },
    {
      path: './display-500.woff2',
      weight: '500',
      style: 'normal',
    },
    {
      path: './display-500.woff2',
      weight: '600',
      style: 'normal',
    },
  ],
});
