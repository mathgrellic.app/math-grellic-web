import { useMemo } from 'react';

import { Lesson } from '#/models/lesson.model';
import { useBoundStore } from './use-store.hook';

type Result = {
  titlePreview: string;
  lesson?: Lesson | null;
};

export function useLessonPreview(): Result {
  const lessonFormData = useBoundStore((state) => state.lessonFormData);

  const lesson = useMemo(() => {
    if (!lessonFormData) {
      return lessonFormData;
    }

    return {
      ...lessonFormData,
      id: 0,
    };
  }, [lessonFormData]);

  const titlePreview = useMemo(
    () => (!!lesson?.title ? `${lesson?.title} Preview` : 'Preview'),
    [lesson],
  );

  return {
    lesson,
    titlePreview,
  };
}
