import { create } from 'zustand';
import { devtools, persist, subscribeWithSelector } from 'zustand/middleware';

import { createCoreSlice } from '#/store/core.store';
import { createLessonSlice } from '#/store/lesson.store';

import type { CoreSlice } from '#/models/core.model';
import type { LessonSlice } from '#/models/lesson.model';

export const useBoundStore = create<CoreSlice & LessonSlice>()(
  devtools(
    persist(
      subscribeWithSelector((...a) => ({
        ...createCoreSlice(...a),
        ...createLessonSlice(...a),
      })),
      {
        name: 'main-storage',
        partialize: (state) => ({
          sidebarMode: state.sidebarMode,
          lessonFormData: state.lessonFormData,
        }),
        skipHydration: true,
      },
    ),
  ),
);
